# Database Performance
We will go into very deep detail for database in this tutorial, we are going to discuss performance issue and most of them is based on PostgreSQL. It's recommended to read [SQL](../sql/) and [Database Design](../db-design/) before reading this tutorial.

In order to learn the following content, please set up the database and data in advanced as following step:
1. Install PostgreSQL and PGAdmin using Docker, refer to [Installation](../sql/README.md#installation).
1. Create a database named `instagram`.
1. Run this [script](./scripts/instagram.sql) to create tables.
1. Follow the steps in this [Data Dumping](../db-design/README.md#data-dumping) to insert the data.

## Data Storage
Let's see how PostgreSQL stores the data first, then go to the performance issue.

### Where is the Data?
We can make use of `SHOW` to pull an individual configuration out of the Postgres database. Use the following command to find where all the data for the database is stored. 
```sql
SHOW data_directory;
```

You should get a path like `/var/lib/postgresql/data`. Now let's get into this container to see what's in there.
```sh
docker exec -it mydb bash
```

Navigate to the path you got before and see what's in this folder.
```sh
cd /var/lib/postgresql/data && ls -ll
```

Our data is all in `/base` folder, we get into this folder and we can find some folders, one folder stores the data for one database.
```sh
cd base && ls -ll
```

We can get the same result using PGAdmin by following query and see which folder represents which database:
```sql
SELECT oid, datname FROM pg_database;
```
<br/><img src="./images/databases.png" width="200"/><br/>

Get into the folder which stores the data of *instagram*, in my case is `16578/` folder. We can see a lot of files in there, we can use the following query to know which file represents which data. For example, `16624` stores all the data for users data, `16670` stores primary key for users table.
```sql
SELECT * FROM pg_class;
```
<br/><img src="./images/pg-class.png" width="600"/><br/>

### Heap Files, Blocks, and Tuples
The file `16624` is called *heap* or *heap file*, which contains all the data of the table. The heap file is divided into many different *blocks* (or called *pages*). Each block/page stores some number of rows, an individual row from the table called *tuple* or *item*. By default, each of blocks/pages are **8KB** in size.
<br/><img src="./images/heap.png" width="900"/><br/>

Let's take a look inside of a block. See the following diagram, Block 1 is being stored on the hard disk of the computer as binary (0's and 1's). So those 0's and 1's represent different information in a block. **Loc of item 1** means this section stores the reference of item 1, the real data of item 1 is stored in **Data for tuple 1** section.
<br/><img src="./images/block.png" width="750"/><br/>

Block 1 is look like:
<br/><img src="./images/binary.png" width="450"/><br/>

To learn more about how PostgreSQL stores data at the binary level, please refer to the [Database Page Layout](https://www.postgresql.org/docs/current/storage-page-layout.html).

## Indexes
See the following query:
```sql
SELECT * FROM users WHERE username = 'Riann';
```

For this query, Postgres has to load many (or all) rows from the heap file to memory then search one by one, it's so called *full table scan*.
<br/><img src="./images/full-table-scan.png" width="600"/><br/>

The full table scan is frequently poor performance. So anytime we see that SQL doing one, we want to investigate and figure out whether there is some way that we can search for this data in some different manner.

Postgres provides a data structure that efficiently tells us what block/index a record stored at, which is called *index*.
<br/><img src="./images/index.png" width="750"/><br/>

While we set a column as index, e.g. username in users table, database extract that property and record the item memory address. It will apply this process for all items and sort in some meaningful way, for example, alphabetical for text, value for numbers, etc.

Database organizes all into a tree data structure and add helpers to the root node, for example, **go left if username is smaller than 'Nancy'**. When database finds the node, this node can point to where that record exists inside the heap file.
<br/><img src="./images/index-tree.png" width="750"/><br/>

There are several different types of index, but 99% of the time we use *B-Tree*, you can check those types in the document [Index Types](https://www.postgresql.org/docs/current/indexes-types.html).

Postgres automatically creates an index for the primary key column and any *unique* constraint of every table, which means we don't need to create for those columns by ourself.

Notice that these indexes don't get listed under **Indexes** in PGAdmin, but we can still use the folllowing command to show all indexes in this database:
```sql
SELECT relname, relkind FROM pg_class WHERE relkind = 'i';
```

### Create/Delete an Index
Use the following command to create and delete an index on `username` column. Notice that when we create an index, PostgreSQL will generate an index using its traditional naming rule, in the following example is `users_username_idx`. You can assign an index custom name but it's still recommended to use default name.
```sql
-- create index
CREATE INDEX ON users (username);

-- delete index
DROP INDEX users_username_idx;
```

To know how fast using index while querying the table, we can use `EXPLAIN ANALYZE` keyword like so:
```sql
EXPLAIN ANALYZE SELECT * FROM users WHERE username = 'Emil30';
```
<br/><img src="./images/explain.png" width="750"/><br/>

Notice that this query only took 0.084 ms to execute. Try to delete the index and use the above again, you will understand how fast to use index on query.

### Downsides of Indexes
You might think that we should add an index to every column of every table, but that's not correct. There are some downsides of indexes:

- Using index takes more space for the tree structure, we can use the following command to print out the size of the hard drive space used by users table and index:
    ```sql
    -- 872 kB
    SELECT pg_size_pretty(pg_relation_size('users'));

    -- 184 kB
    SELECT pg_size_pretty(pg_relation_size('users_username_idx'));
    ```
- Index slows down insert/update/delete operations, because the index has to be updated.
- Index might not actually get used by Postgres.

## Query Tuning
There are four steps of the query processing pipeline in the Postres.
1. Parser: It will make sure the query is a valid SQL. After evaluating this query, it will build up a *query tree* which is a programmatic description of the query that you are trying to run.
1. Rewrite: Decompose views into underlying table references (we will explain it later). It takes the query tree and make a couple of small adjustments to it.
1. Planner: It's try to figure out what information you are trying to fetch based on query tree, and then come up with a series of different plans or strategies that could be used to get that information. The planner will determine which one is fastest and then choose that plan.
1. Execute: Run the plan.

### Explain & Explain Analyze
`EXPLAIN` keyword is to build a *query plan* and display information about it. `EXPLAIN ANALYZE` shows a *query plan*, **runs it** and shows statistics about the execution.

We are only ever going to use `EXPLAIN` and `EXPLAIN ANALYZE` while we are trying to do some performance evaluation. We are never going to leave these in any production application.

Type following commands to see the *query plan*:
```sql
-- explain
EXPLAIN SELECT username, contents
FROM users
JOIN comments ON comments.user_id = users.id
WHERE username = 'Alyson14';

-- explain analyze
EXPLAIN ANALYZE SELECT username, contents
FROM users
JOIN comments ON comments.user_id = users.id
WHERE username = 'Alyson14';
```

<br/><img src="./images/query-plan-1.png" width="500"/><br/>
<br/><img src="./images/query-plan-2.png" width="750"/><br/>

For the PGAdmin, you can also use explain analyze feature. First of all, remove `EXPLAIN` or `EXPLAIN ANALYZE` in front of you query, and click the following button you can see the nice UI for explain analyze.
<br/><img src="./images/explain-analyze-button-1.png" width="300"/><br/>

You can also select all features as follows and click explain analyze button.
<br/><img src="./images/explain-analyze-button-2.png" width="400"/><br/>
<br/><img src="./images/pgadmin-explain.png" width="1050"/><br/>

### Details for Explain Analyze
Let's take a look for the result of explain analyze:
```
Hash Join  (cost=8.31..1756.11 rows=11 width=81) (actual time=4.123..935.470 rows=7 loops=1)
    Hash Cond: (comments.user_id = users.id)
    ->  Seq Scan on comments  (cost=0.00..1589.10 rows=60410 width=72) (actual time=0.011..459.029 rows=60410 loops=1)
    ->  Hash  (cost=8.30..8.30 rows=1 width=17) (actual time=0.051..0.068 rows=1 loops=1)
    Buckets: 1024  Batches: 1  Memory Usage: 9kB
    ->  Index Scan using users_username_idx3 on users  (cost=0.28..8.30 rows=1 width=17) (actual time=0.021..0.035 rows=1 loops=1)
    Index Cond: ((username)::text = 'Alyson14'::text)
Planning Time: 0.164 ms
Execution Time: 935.563 ms  
```

Each of the rows that have an arrow on them, we refer to as a *query node*. This is some step where we are trying to access some data that is stored inside the database or do some processing. In fact, the very top of line up (Hash Join ...) is also a query note as well. The way we read this is by going to the innermost rows, so in this case is:
<br/><img src="./images/steps.png" width="300"/><br/>

Every single place where we see these arrows, we can imaging it's trying to access some data inside of the database or index, and then it emits that data or pass that data up next to the nearest parent that has an arrow on it for the index.

Let's take a look for first line above:
<br/><img src="./images/meaning.png" width="900"/><br/>

The interesting thing is rows and width in above diagram. Before accessing the table, how Postgres guesses that data? It's based on some statistics data. For example, use following command to show some statistics information about users table.
```sql
SELECT * FROM pg_stats WHERE tablename = 'users';
``` 

### Costs
Cost is about how expensive this step is relative to other steps of the query plan or to be able to compare one query plan versus other. The cost is calculated by the following rule, you can get more details about cost in [Planner Cost Constants](https://www.postgresql.org/docs/current/runtime-config-query.html#RUNTIME-CONFIG-QUERY-CONSTANTS).
```
cost = (# pages read sequentially) * seq_page_cost
      + (# pages read at random) * random_page_cost
      + (# rows scanned) * cpu_tuple_cost
      + (# index entries scanned) * cpu_index_tuple_cost
      + (# times function/operator evaluated) * cpu_operator_cost
      + ... 
```

There is no unit for cost, but the `random_page_cost` default value is 1, so 1 cost means the cost to read one sequential page more or less. For example, see the following calculation for **Seq Scan on comments**:
```
We have 985 pages, 60,410 rows in comments table.

Processing a single row is really cheap compared to loading an entire page, so page factor is 1, and row factor is 0.01.

cost = (# pages) * seq_page_cost + (# rows) * cpu_tuple_cost
= 985 * 1 + 60,410 * 0.01
= 1589.1
```

You might notice that there are two number in the cost, in case of **Seq Scan on comments**, the cost is `0.00..1589.10` which means 0.00 and 1589.10. The first number is the **cost for this step to produce the first row**, second one is the **cost for this step to produce all rows**.

In **Seq Scan on comments** case, first number is 0.00 which means the first row can be processed or emitted to next step without waiting whole step finished. But let's another example **Hash  (cost=8.30..8.30)**, which means it can be emitted to next step after whole data is processed in this step.

We found that the startup cost of the top note (**Hash Join**) should be 0, but it's 8.30. That's because we calculate the cost of the parent node as the sum of all child nodes.

Postgres will choose the best query plan for us, which means it will not use index if the cost of index is more expensive. Let's see the following example, there are 63,071 rows in the result of the query.
```sql
SELECT * FROM likes WHERE created_at < '2013-01-01';
```

We know that Postgres query the data using **Seq Scan**.
```sql
EXPLAIN SELECT * FROM likes WHERE created_at < '2013-01-01';
```

So we create an index on `created_at`:
```sql
CREATE INDEX ON likes (created_at);
```

Then we use `EXPLAIN` again to see the query plan, we found that Postgres uses index this time. But how about the following query?
```sql
EXPLAIN SELECT * FROM likes WHERE created_at > '2013-01-01';
```

Postgres still uses **Seq Scan** because there are too many data is created after '2013-01-01', so Postgres compare to different query plans and it found that **Seq Scan** is more efficient. This is a good example to show that Postgres might not use index every time.

To prevent above situation, every time we want to use index, we should test the query we want to use and make sure index is used by Postgres. If not, we should drop the index on that column.

## Common Table Expressions (CTE)
Try to show the username of users who were tagged in a caption or photo before January 7th, 2010. Also show the date they were tagged. We can build a query using `UNION` like so:
```sql
SELECT username, tags.created_at
FROM users
JOIN (
  SELECT user_id, created_at FROM caption_tags
  UNION ALL
  SELECT user_id, created_at FROM photo_tags
) AS tags ON tags.user_id = users.id
WHERE tags.created_at < '2010-01-07';
```

### Simple CTE
Sometimes we want tags table more clear and readable, we can use `WITH AS` keyword which is called *common table expression, CTE*. We can refactor the above query as follows:
```sql
WITH tags AS (
  SELECT user_id, created_at FROM caption_tags
  UNION ALL
  SELECT user_id, created_at FROM photo_tags
)

SELECT username, tags.created_at
FROM users
JOIN tags ON tags.user_id = users.id
WHERE tags.created_at < '2010-01-07';
```

CTE produces a table that we can refer to anywhere else. There are two forms of CTE, one is simple form used it make a query easier to understand like above example. Another is recursive form used to write queries that are otherwise impossible to write.

### Recursive CTE
Recursive CTE is useful anytime you have a tree or graph-type data structure. It **must** use a `UNION` keyword, notice that simple CTE's don't have to use a `UNION`. Here's the first example:
```sql
WITH RECURSIVE countdown(val) AS (
  SELECT 3 AS val  -- initial, Non-recursive query
  UNION
  SELECT val - 1 FROM countdown WHERE val > 1 -- recursive query
)

SELECT *
FROM countdown;
```

Let's explain the whole process in this example. There are two tables behind the scene while database runs the recursive CTE, one is called *result table*, another is *working table*. Let's go step by step:
1. `countdown` is a table name, it has a column called `val`. You can define multiple columns like `countdown(val, count, created_at)`. It also defines working table and result table schema, they have a column called `val` as well.
    ```
    results table | working table
    --------------+--------------
         val      |      cal
    ```
1. Run the initial non-recursive statement and put the results into the results table and working table.
    ```
    results table | working table
    --------------+--------------
         val      |      cal
    --------------+--------------
          3       |       3
    ```
1. Run the recursive statement replacing tha table name `countdown` with a reference to the working table. So it's like `SELECT val - 1 FROM (working table) WHERE val > 1`, what we got is 2.
1. If recursive statement returns some rows, append them to the results table.
    ```
    results table | working table
    --------------+--------------
         val      |      cal
    --------------+--------------
          3       |       3
          2       |       
    ```
1. Drop working table then put the result in working table.
    ```
    results table | working table
    --------------+--------------
         val      |      cal
    --------------+--------------
          3       |       2
          2       |       
    ```
1. Run step 4 and 5 again.
    ```
    results table | working table
    --------------+--------------
         val      |      cal
    --------------+--------------
          3       |       1
          2       |       
          1       |       
    ```
1. If recursive statement returns no rows then stop recursion, return the results table.
    ```
    | countdown |
    |-----------|
    |    val    |
    |-----------|
    |     3     |
    |     2     |
    |     1     |
    ```

We give you a real example. Imagine that the Instagram provide a recommend follow list is based on who you are following, then they recommend the people who is followed by the people you follow, so on and so on. For example, if A follows B, B follows C, then Instagram recommends A follows C like so. If there are many layers for this recommend system, it's a graph data structure and it's recursive.

See the following digram, **Hallie** is following **The Rock** and **Kevin Hart**. **The Rock** is following **Snoop Dog**, and **Kevin Hart** is following **Justin Beiber** and **Snoop Dog**, so on and so on.
<br/><img src="./images/recursive-follow.png" width="500"/><br/>

Let's create a query to find out who are recommended for `user_id = 1,000` in this system.
```sql
WITH RECURSIVE suggestions(leader_id, follower_id, depth) AS (
  SELECT leader_id, follower_id, 1 AS depth
  FROM followers
  WHERE follower_id = 1000
  UNION
  SELECT followers.leader_id, followers.follower_id, depth + 1
  FROM followers
  JOIN suggestions ON suggestions.leader_id = followers.follower_id
  WHERE depth < 3
)

SELECT DISTINCT users.id, users.username
FROM suggestions
JOIN users ON users.id = suggestions.leader_id
WHERE depth > 1
LIMIT 5;
```

## Views
Let's try to show the users who were tagged the most, here's our solution:
```sql
SELECT username, COUNT(*)
FROM users
JOIN (
  SELECT user_id FROM photo_tags
  UNION ALL
  SELECT user_id FROM caption_tags
) AS tags ON tags.user_id = users.id
GROUP BY username
ORDER BY COUNT(*) DESC;
```

You might notice that we have to find the `UNION` several times while query above kind of issue. There's no benefit to keeping these records in separate tables, we might guess that we have a bad design. There are two possible ways to fix this up.

The first solution is to merge two tables and delete original tables. So we can create a single `tags` table, and copy all the rows from `photo_tags` and `caption_tags` tables. But we got two issues in this solution:
1. We can't copy over the ID's of photo_tags and caption_tags since they must be unique.
1. If we delete original tables, we break any existing queries that refer to them.

The second solution is creating a *view* which is a correct solution, you can think of view as a fake table that has a reference to rows or data from other data inside of the database. View doesn't actually create a new table or move any data around. The following query to show up how to create a view.
```sql
CREATE VIEW tags AS (
  SELECT id, created_at, user_id, post_id, 'photo_tag' AS type FROM photo_tags
  UNION ALL
  SELECT id, created_at, user_id, post_id, 'caption_tags' AS type  FROM caption_tags
);
```

Now we can use this view as a table to query:
```sql
SELECT username, COUNT(*)
FROM users
JOIN tags ON tags.user_id = users.id
GROUP BY username
ORDER BY COUNT(*) DESC;
```

When to use a view? We should think the use cases. For our case, the 10 most recent posts are important. For example, show the users who created the 10 most recent posts or show the number of likes each of the 10 most recent posts received. So we can create a view for 10 most recent posts like so:
```sql
CREATE VIEW recent_posts AS (
  SELECT *
  FROM posts
  ORDER BY created_at DESC
  LIMIT 10
);  
```

### Changing Views
To create or change a view you can use the following command:
```sql
CREATE OR REPLACE VIEW recent_posts AS (
  SELECT *
  FROM posts
  ORDER BY created_at DESC
  LIMIT 15
);
```

To remove a view, use the following command:
```sql
DROP VIEW recent_posts;
```

### Materialized Views
A *materialized views* is from a query that gets executed only at very specific times, but the results are saved and can be referenced without rerunning the query.

Here's an example: For each week, show the number of likes that posts and comments received. Use the post and comment `created_at` date, not when the like was received.
<br/><img src="./images/materialized-view.png" width="750"/><br/>

We are going to use left join to get the data structure in this case, see the following query and diagram.
```sql
SELECT *
FROM likes
LEFT JOIN posts ON posts.id = likes.post_id
LEFT JOIN comments ON comments.id = likes.comment_id;
```
<br/><img src="./images/left-join.png" width="750"/><br/>

To get the result for each week, we can use the following command:
```sql
SELECT
  date_trunc('week', COALESCE(posts.created_at, comments.created_at)) AS week,
  COUNT(posts.id) AS num_likes_for_posts,
  COUNT(comments.id) AS num_likes_for_comments
FROM likes
LEFT JOIN posts ON posts.id = likes.post_id
LEFT JOIN comments ON comments.id = likes.comment_id
GROUP BY week
ORDER BY week;
```

But above query takes us too much time. So we can use the materialized view to hold this data which is like a cache.
```sql
CREATE MATERIALIZED VIEW weekly_likes AS (
  SELECT
    date_trunc('week', COALESCE(posts.created_at, comments.created_at)) AS week,
    COUNT(posts.id) AS num_likes_for_posts,
    COUNT(comments.id) AS num_likes_for_comments
  FROM likes
  LEFT JOIN posts ON posts.id = likes.post_id
  LEFT JOIN comments ON comments.id = likes.comment_id
  GROUP BY week
  ORDER BY week
) WITH DATA;
```

We can get the data very quickly next time:
```sql
SELECT * FROM weekly_likes;
```

But notice that because this is a cache data which means it might be outdated. For example, if we delete some rows like:
```sql
DELETE FROM posts WHERE created_at < '2010-02-01';
```

Now we query `weekly_likes` again we will find nothing changes. To update the materialized view, we can use the following command:
```sql
REFRESH MATERIALIZED VIEW weekly_likes; 
```

So we only use materialized view to get the data which is not changed very often. Our case is a good example, because we don't need to run the query again until next week.

## Transactions
Imagine a scenario that our database needs to transfer $50 from Alyson to Gia. So we can separate it into two steps, first is to withdraw $50 from Alyson's account, second is to add $50 to Gia's account. But how about the system is crashed after first step is finished, it will make the data inconsistence.

So we need both these queries to be successfully executed or if only one is able and not the other, we need to roll them all back and undo all the changes we have made. We will use *transaction* to solve this problem.

Let's create some sample data to learn transactions.
```sql
CREATE TABLE accounts (
  id SERIAL PRIMARY KEY,
  name VARCHAR(20) NOT NULL,
  balance INTEGER NOT NULL
);

INSERT INTO accounts (name, balance)
VALUES
  ('Gia', 100),
  ('Alyson', 100);
```

To open a transaction by running just a keyword `BEGIN`. Before you run any command, you can notice that there's a small icon at the top of the query tool then click, it will show up "The session is idle and **there is no current transaction**."
<br/><img src="./images/icon-1.png" width="300"/><br/>

Then run the following command.
```sql
BEGIN;
```

You will find that the icon is changed and shows up "The session is idle **in a valid transaction block**."
<br/><img src="./images/icon-2.png" width="300"/><br/>

Now let's run the following command:
```sql
UPDATE accounts
SET balance = balance - 50
WHERE name = 'Alyson';
```

Query the accounts table in this query tool, you will find that the balance of Alyson is $50.
```sql
SELECT * FROM accounts;
```

But when you use another query tool to query this table again, you will see that the balance of Alyson is still $100. That's because there are two different connections (sessions) for database, and they are isolated (database doesn't copy anything behind the scene, but we can image it copies the data for the workspace of each connection).
<br/><img src="./images/connections.png" width="450"/><br/>

So we can use transaction to do some temporary operations, let's add transfer this $50 to Gia like so:
```sql
UPDATE accounts
SET balance = balance + 50
WHERE name = 'Gia';
```

After temporary operations, there are four scenarios:
1. We can run `COMMIT` to merge changes back into main data pool.
    ```sql
    COMMIT;
    ```
1. If we don't want to merge these changes, we can run `ROLLBACK` to dump all pending changes and delete this separate workspace.
1. Running bad command will put the transaction in an *aborted* state then we must rollback manually to get out of this state.
    1. Let's test this scenario. First of all, reset the table.
        ```sql
        UPDATE accounts SET balance = 100;
        ```
    1. Use following bad command to get an error:
        ```sql
        BEGIN;
        SELECT * FROM abc;
        ```
    1. Try to use a valid command, you will find you cannot do anything because you are in aborted state.
        ```sql
        SELECT * FROM accounts;
        ```
    1. Let's roll back to get out of this state.
        ```sql
        ROLLBACK;
        ```
1. Losing the connection (crashing) will automatically rollback the transaction.
    1. Reset the table.
        ```sql
        UPDATE accounts SET balance = 100;
        ```
    1. Update value in a transaction.
        ```sql
        BEGIN;
        UPDATE accounts
        SET balance = balance - 50
        WHERE name = 'Alyson';
        ```
    1. Let's try to delete the connection. Go to dashboard, you can find out a connection which state is **idle in transaction**. Click the left red close button to terminate that session.
    <br/><img src="./images/idle-in-transaction.png" width="900"/><br/>
    1. You can use original query tool or a new query tool to query accounts table. You will find that the data is rollback automatically.
        ```sql
        SELECT * FROM accounts;
        ```

