# Databases

This is a study note for learning databases.

There are some content included in this note:
- [Database topics](./db-topics/)
- [SQL](./sql/)
- [Database design](./db-design/)
- [Performance](./performance/)
- [Migrations](./migrations/)
- [Node API with SQL](./node-api/)

## Reference
[SQL and PostgreSQL: The Complete Developer's Guide](https://www.udemy.com/course/sql-and-postgresql/)
