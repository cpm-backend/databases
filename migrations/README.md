# Migration

## Schema migration
Schema migration is making changes to your database structure. To update a column for database, it's a really challenge work. For example, if we want to rename a column from `contents` to `body` like so, it's really easy to finish in the database. But after updating the database, we will get an error immediately from our application because it's still using the old column name! In other words, **changes to the database structure and changes to clients need to be made at precisely the same time**.

Some company plans or schedules downtime changes to the database structure, this is frequently what they are doing during those down times of application. However there are many companies where you need to make changes to the database and just plan cannot take take the application down (e.g. the company has service level agreement, SLA).

Another issue is **when working with other engineers we need a really easy way to tie the structure of our database to our code**. For example, if we update the related code for the application and create a PR. While other engineers try to download this code to test in their local database, they will find an error because their database is still using old column name.

### Migration Files
Schema migration file is a code that describes a precise change to make to the database. Migration file is not tied to any particular language, you can use Python, Java, JavaScript and so on. There are two parts of migration file, *up* section and *down* section.
<br/><img src="./images/migration-file.png" width="450"/><br/>

Up section contains a statement that advances, or upgrades the structure of the database. Down section contains a statement that exactly undo's the up command.
<br/><img src="./images/apply-revert.png" width="600"/><br/>

Using migration file we can solve the issues easily.
- **Changes to the database structure and changes to clients need to be made at precisely the same time**: As soon as new version of the API is ready to start receiving traffic, we can then automatically run all available migrations inside of our project. The very instant that our database structure is changed, we can then start receiving traffic on this new version of the API. This cloud shrink down that window of time where we have a different version of our API or database structure down to a very small period of time. Notice that it doesn't solve all issues, we will discuss it later.
    <br/><img src="./images/api-deploy.png" width="600"/><br/>
- **When working with other engineers we need a really easy way to tie the structure of our database to our code**: In this case, other engineer can *apply* the migration in their local database. After reviewing the code, they can *revert* the migration so that they are back to the current real structure of the database.

Here are some libraries for creating schema migrations:
- Java:
    - flywaydb.org
- Javascript:
    - npmjs.com/package/node-pg-migrate
    - npmjs.com/package/typeorm
    - npmjs.com/package/sequelize
    - npmjs.com/package/db-migrate
- Python:
    - pypi.org/project/alembic
    - pypi.org/project/yoyo-migrations
- Go:
    - github.com/golang-migrate/migrate
    - github.com/go-pg/migrations
    - gorm.io/docs/migration.html

We will use [node-pg-migrate](https://www.npmjs.com/package/node-pg-migrate) to create migrations. Many migration tools can automatically generate migrations for you, but you may want to write all migrations manually using plain SQL.

### Creating Migrations
Go to folder [social-network/](./social-network/), and run `npm install` to get node modules. You might need to install *node.js* first. 

Take a look for [package.json](./social-network/package.json), you can see the script as follows. 
```json
{
  "scripts": {
    "migrate": "node-pg-migrate"
  }
}
```

We can run the following command under `social-network/` folder in your terminal to create a migration file under [migration/](./social-network/migrations/) folder (refer to this [file](./social-network/reference-migrations/1653054846405_table-comments.js)).
```
npm run migrate create table comments
```

Let's put some SQL in this file:
```js
exports.shorthands = undefined;

exports.up = pgm => {
  pgm.sql(`
    CREATE TABLE comments (
      id SERIAL PRIMARY KEY,
      created_at TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP,
      updated_at TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP,
      contents VARCHAR(240) NOT NULL
    );
  `);
};

exports.down = pgm => {
  pgm.sql(`
    DROP TABLE comments;
  `);
};
```

In order to learn the following content, please set up the database and data in advanced as following step:
1. Install PostgreSQL and PGAdmin using Docker, refer to [Installation](../sql/README.md#installation).
1. Create a database named `socialnetwork`.

Then go to your terminal again and type following command to create a migration.
```
DATABASE_URL=postgres://postgres:[YOUR_PASSWORD]@localhost:5432/socialnetwork npm run migrate up
```
<br/><img src="./images/migration-1.png" width="900"/><br/>

You can find there are two tables in `socialnetwork` database now, one is comments table, another is `pgmigrations` table which is created by `node-pg-migrate` module automatically. It keep track of what migrations have been executed amd make sure that you don't run that same migration again.

For instance, let's try to run the same above command again, you can find that it won't be executed second time.
<br/><img src="./images/migration-2.png" width="900"/><br/>

If you want to revert this change, we can use the following command to execute *down* section in migration file.
```
DATABASE_URL=postgres://postgres:[YOUR_PASSWORD]@localhost:5432/socialnetwork npm run migrate down
```

Let's see create another migration (you can refer to this [file](./social-network/reference-migrations/1653060472349_rename-contents-to-body.js)).
```
npm run migrate create rename contents to body
```
```js
exports.shorthands = undefined;

exports.up = pgm => {
  pgm.sql(`
    ALTER TABLE comments
    RENAME COLUMN contents TO body;
  `);
};

exports.down = pgm => {
  pgm.sql(`
    ALTER TABLE comments
    RENAME COLUMN body TO contents;
  `);
};
```

Now let's run the following command to apply migrations.
```
DATABASE_URL=postgres://postgres:[YOUR_PASSWORD]@localhost:5432/socialnetwork npm run migrate up
```

Because we reverted the first migration before, so we can see it applies two files in this command. But if we do it down, it's only going to **revert one step at a time**.
```
DATABASE_URL=postgres://postgres:[YOUR_PASSWORD]@localhost:5432/socialnetwork npm run migrate down
```

## Data Migration
Let's imagine a scenario which we decide to take the latitude and logitude columns and merge them down into one single column of type `POINT` which is a data type in Postgres for expressing an x, y coordinate.
<br/><img src="./images/point.png" width="500"/><br/>

Data migrations mean we need to move data around. So here's our steps:
1. Add column `loc` -> schema migration
1. Copy (`lat`, `lng`) to `loc` -> data migration
1. Drop columns `lat` and `lng` -> schema migration

### Single Migration Issues
Notice that there are several reasons to **not run data migrations at the same time as schema migrations**. We will focus on one reason as follows.

If we want to put everything in single migration, using transaction might be a good idea.
<br/><img src="./images/single-migration.png" width="850"/><br/>

While we are doing the copy operation, we still have our API server up and we are still accepting requests and creating posts. Because inside the transaction, we essentially have the snapshot of the post table. Copying data needs a lot of time, it could take several minutes or hours so the real posts table might be very different with the snapshot.
<br/><img src="./images/t-1.png" width="700"/><br/>
<br/><img src="./images/t-2.png" width="700"/><br/>

For the data created within migration operation, we lost its `lat` and `lng` information.
<br/><img src="./images/t-3.png" width="700"/><br/>

So the correct steps as follows:
1. Add column `loc` (in schema migration file).
1. Deploy new version of API that will write values to both `lat`, `lng` and `loc`.
1. Copy (`lat`, `lng`) to `loc` (we could write a separate script to do this step, not necessary in the form of schema migration file).
1. Update code to only write to `loc` column.
1. Drop columns `lat` and `lng` (in schema migration file).

We will use above steps to practice data migration in next section.

### Data Migration Example
Create a new migration file (refer to this [file](./social-network/reference-migrations/1653207644302_add-posts-table.js)) to add post table in `socialnetwork` database.
```
npm run migrate create add posts table
```
```js
exports.shorthands = undefined;

exports.up = pgm => {
  pgm.sql(`
    CREATE TABLE posts (
      id SERIAL PRIMARY KEY,
      url VARCHAR(300),
      lat NUMERIC,
      lng NUMERIC
    )
  `);
};

exports.down = pgm => {
  pgm.sql(`
    DROP TABLE posts;
  `);
};
```

Run the following command to create a migration:
```
DATABASE_URL=postgres://postgres:[YOUR_PASSWORD]@localhost:5432/socialnetwork npm run migrate up
```

We build a small web server to practice data migrations. Go to [social-network/](./social-network/) folder and open the [index.js](./social-network/index.js). Search for string `YOUR_PASSWORD` and replace it by your password.

Then run the following command in your terminal to launch this web server:
```
node index.js
```

Then visit http://localhost:3005, you should see this small web server. Try to add some data using your browser. Then we can go through the process of data migrations.

1. Go to [social-network/](./social-network/) folder and run the following command to create a schema migration file (refer to this [file](./social-network/reference-migrations/1653209623748_add-loc-to-posts.js)).
    ```
    npm run migrate create add loc to posts
    ```
1. Write the following code in the migration file created in previous step to add a new column `loc` to posts table.
    ```js
    exports.shorthands = undefined;

    exports.up = pgm => {
      pgm.sql(`
        ALTER TABLE posts
        ADD COLUMN loc POINT;
      `);
    };

    exports.down = pgm => {
      pgm.sql(`
        ALTER TABLE posts
        DROP COLUMN loc;
      `);
    };
    ```
1. Run the following command to create migrations:
    ```
    DATABASE_URL=postgres://postgres:[YOUR_PASSWORD]@localhost:5432/socialnetwork npm run migrate up
    ```
1. Restart your web server and visit http://localhost:3005.
    ```
    node index.js
    ```
1. Try to add some data, you should not see any error.
1. Update [index.js](./social-network/index.js).
    ```js
    app.post('/posts', async (req, res) => {
      const { lng, lat } = req.body;

      await pool.query('INSERT INTO posts (lat, lng, loc) VALUES ($1, $2, $3);', [lat, lng, `(${lng},${lat})`]);

      res.redirect('/posts');
    });
    ```
1. Restart your web server and visit http://localhost:3005. Then try to insert some data.
    ```
    node index.js
    ```
1. Go to your PGAdmin, you should see the results like so:
    <br/><img src="./images/point-result-1.png" width="500"/><br/>

### Updating Data Solutions
To updating the data, it's kind of complicated. There are two possible solutions.
- Determine updates in JS: We can get a lot of posts and run business logic and validation in JS. Finally update data in our database.
    - Node server could crash if we try to load up millions of posts all at once, we should use batching.
    - It requires us to manually connect to the database from a Node environment.
    - One upside is that we can run complex business logic or validation on the records before doing the update.
- Using SQL directly: We could write out some JS file and connect to our database and just send in a very simple update statement. Or we could just write out that statement directly inside PGAdmin and execute it.
    - No moving information between database and Node which means it executes very fast.
    - It's harder to implement validation and business logic.

Notice that transaction issue around both solutions. Because batching could fail halfway through, leaves us in a halfway-between state. So we might want to run the entire update inside of one single transaction.

But whenever the transaction updates a value, that entire row is going to get locked which means another transaction cannot update this value. In other word, if we want to use one single transaction for all batching, some rows will be locked quite a long time.

Now let's use JS file to run a plain SQL to update the table.
1. Open [01-lng-lat-to-loc.js](./social-network/migrations/data/01-lng-lat-to-loc.js) file and search for string `YOUR_PASSWORD` and replace it by your password.
1. Run the following command to update the data:
    ```
    node migrations/data/01-lng-lat-to-loc.js
    ```
1. Now you can check PGAdmin, the value of loc column should not be `NULL` for all rows.
1. Update [index.js](./social-network/index.js) to operate `loc` column with database.
    ```js
    app.get('/posts', async (req, res) => {
      res.send(`
          ...
          <tbody>
            ${rows
              .map((row) => {
                return `
                <tr>
                  <td>${row.id}</td>
                  <td>${row.loc.x}</td>
                  <td>${row.loc.y}</td>
                </tr>
              `;
              })
              .join('')}
          </tbody>
          ...
      `);
    });

    app.post('/posts', async (req, res) => {
      const { lng, lat } = req.body;

      await pool.query('INSERT INTO posts (loc) VALUES ($1);', [`(${lng}, ${lat})`]);

      res.redirect('/posts');
    });
    ```
1. Go back to [social-network/](./social-network/) folder and restart your web server and visit http://localhost:3005. Then try to insert some data.
    ```
    node index.js
    ```
1. Go to your PGAdmin, you should see the results like so:
    <br/><img src="./images/point-result-2.png" width="500"/><br/>
1. Create a new migration file to drop columns (refer to this [file](./social-network/reference-migrations/1653214481172_drop-lng-and-lat-from-posts.js)).
    ```
    npm run migrate create drop lng and lat from posts
    ```
1. Write the following code in the migration file created in previous step to drop columns from posts table.
    ```js
    exports.shorthands = undefined;

    exports.up = pgm => {
      pgm.sql(`
        ALTER TABLE posts
        DROP COLUMN lat,
        DROP COLUMN lng;
      `);
    };

    exports.down = pgm => {
      pgm.sql(`
        ALTER TABLE posts
        ADD COLUMN lat NUMERIC,
        ADD COLUMN lng NUMERIC;
      `);
    };
    ```
1. Run the following command to create migrations:
    ```
    DATABASE_URL=postgres://postgres:[YOUR_PASSWORD]@localhost:5432/socialnetwork npm run migrate up
    ```
1. Visit http://localhost:3005 and try to insert some data. Everything should work as usual.
