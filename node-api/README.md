# Node API with SQL
In order to learn the following content, please set up the database and data in advanced as following step:
1. Install PostgreSQL and PGAdmin using Docker, refer to [Installation](../sql/README.md#installation).
1. Create a database named `socialrepo`.

## Accessing PostgreSQL from API's
In this section, we are focusing more on how we actually interface with the database as opposed to putting together an API.

1. Go to [social-repo/](./social-repo/) folder and install modules.
    ```
    npm install
    ```
1. Create a migration file to add a new table (refer to this [file](./social-repo/migrations/1653224651636_add-users-table.js)).
    ```
    npm run migrate create add users table
    ```
1. Write the following code in the migration file created in previous step to drop columns from posts table.
    ```js
    exports.shorthands = undefined;

    exports.up = pgm => {
      pgm.sql(`
        CREATE TABLE users (
          id SERIAL PRIMARY KEY,
          created_at TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP,
          updated_at TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP,
          bio VARCHAR(400),
          username VARCHAR(30) NOT NULL
        );
      `);
    };

    exports.down = pgm => {
      pgm.sql(`
        DROP TABLE users;
      `);
    };
    ```
1. Run the following command to create a migration:
    ```
    DATABASE_URL=postgres://postgres:[YOUR_PASSWORD]@localhost:5432/socialrepo npm run migrate up
    ```

### Connection Pools
[pg](https://www.npmjs.com/package/pg) module is used to specifically set up a connection to Postgres and run some SQL. *pg* module can be used to create *client*, we usually do not make use of client directly. Because a client can only be used for one query at a time.

Rather than using a client directly, we generally make use of something instead called a *pool*. A pool internally maintains a list of different clients. Then any time that you need to run a query, you essentially ask the pool to run a query for you. The pool will take your query handed off to one of the clients that is free internally, and that client will execute the query over to Postgres.
<br/><img src="./images/pool.png" width="500"/><br/>

There is one scenario in which you will make use of a client directly, that is whenever you need to write ro run a transaction.

Note: Please take a look for [pool.js](./social-repo/src/pool.js) which is for connecting the database. Normally, you will see the connect method in official document like so:
```js
const pg = require('pg');

// NORMALLY, we would create a pool like this
const pool = new pg.Pool({
  host: 'localhost',
  port: 5432
});

module.exports = pool;
```

But we won't use above structure because it's challenging to connect to multiple different databases for test.

Please search for `YOUR_PASSWORD` in [pool.js](./social-repo/src/pool.js) and replace it with you password. Then go to next section.

### The Repository Pattern
We need only one central point for accessing a type of resource inside the database. For example, we create a user repository to govern users table. This repository can be implemented as an object with plain functions, as an instance of a class or anything, only just one is fine.
<br/><img src="./images/repo-pattern.png" width="900"/><br/>

First of all, let's insert some data into your database. Go to PGAdmin and run the following query in `socialrepo` database.
```sql
INSERT INTO users (bio, username)
VALUES
  ('This is my bio', 'Alyson14'),
  ('This is about me!', 'Gia67');
```

Then go to your terminal and run the following command to start the web server at [social-repo/](./social-repo/) folder.
```
npm run start
```

After server's up, you can download the [Postman](https://www.postman.com/downloads/) and test the APIs. For example, use `GET http://localhost:3005/users` to get all users.
<br/><img src="./images/postman-find.png" width="450"/><br/>

### Casing Issues
The naming rule in SQL world is *snake case*, e.g. `created_at`. But the naming rule in JS is *camel case*, e.g. `createdAt`. So we can create a function to transfer snake case to camel case.

please refer to [utils/to-camel-case.js](./social-repo/src/repos/utils/to-camel-case.js) as follows:
```js
module.exports = rows => {
  return rows.map(row => {
    const replaced = {};

    for (let key in row) {
      const camelCase = key.replace(/([-_][a-z])/gi, ($1) => 
        $1.toUpperCase().replace('_','')
      );
      replaced[camelCase] = row[key];
    }

    return replaced;
  });
};
```

### Postman Quick Tests
```sh
## Find all users
GET http://localhost:3005/users

## Find an user
GET http://localhost:3005/users/1

## Insert a new user
POST http://localhost:3005/users
{
  "username": "minty3",
  "bio": "I am a mint"
}

## Update an user
PUT http://localhost:3005/users/1
{
  "username": "Alyson156",
  "bio": "I am alyson"
}

## Delete an user
DELETE http://localhost:3005/users/1
```

## SQL Injection Exploits
Let's see how to implement the method `findById()` in [user-repo.js](./social-repo/src/repos/user-repo.js). First idea might be as follows:
```js
class UserRepo {
  static async findById(id) {
    // WARNING: really big security issue!
    const { rows } = await pool.query(`SELECT * FROM users WHERE id = ${id};`);
    return toCamelCase(rows)[0];
  }
}
```

Of course we can test it on our Postman really easy like `GET http://localhost:3005/users/1`. There is no error, so what's the issue? How about the following query?
```
GET http://localhost:3005/users/1;DROP TABLE users;
```

After running above command, you will find that users table is dropped. This is so called *SQL injection exploits*, it's a huge security issue.

So we **never, ever directly concatenate user-provided input into a SQL query**. There are a variety of safe ways to get user-provided values into a string:
- Rely on Postgres to sanitize values for us.
- Add code to sanitize user-provided values to our app.

### Prepared Statement
So the first solution is using *prepared* statements. We can break above dangerous process into a two step process. We will give the `pg` module two separate things, statement and values.

`pg` creates what is called a *prepared* statement with some random name. It will reach out the database and send prepared statement to it and tell database to run this query at some point time very soon. Then `pg` will execute the prepared statement to ask database substitute the `$1`.
<br/><img src="./images/prepared.png" width="900"/><br/>

Postgres knows that when you call execute and you provide some values for prepared statement, these are values that should be placed into a query. If you ever try to put in something like above query, it will be replaced inside of the function like:
```sql
PREPARE asdfasdf (INTEGER) AS
  SELECT id, username, bio
  FROM users
  WHERE id = "1;DROP TABLE users";
```

So that would not actually get executed in any way.

But the downside of this solution is we can only use a prepared statement when we are trying to substitute in a **value** to a query. We cannot specify identifiers (e.g. column name, table name and so on) on the fly with a prepared statement.

Let's see what happened when we use bad query on prepared statement, see the method `findById()` in [user-repo.js](./social-repo/src/repos/user-repo.js). We will get the error message as follows:
<br/><img src="./images/sql-injection.png" width="900"/><br/>

## Parallel Testing
Imagine we have some test files to make sure that our application is working as expected. We will use a test runner called *Jest* which is a program that's going to execute these files all **at the exact same time**. Here's a big challenge, we can very easily get into scenarios where these different test files have little conflicts going on between them. Because these tests are being executed in parallel, they might interfere with each other and cause some test to fail erroneously.

Go to [package.json](./social-repo/package.json), you will notice that we use `--no-cache` to make sure to run test files in parallel.
```json
{
  "scripts": {
    "test": "jest --no-cache"
  }
}
```

### Multi-database Setup
To test the functionality of application, we have to create a second database called `socialrepo-test`. We cannot interact with production database for testing. Here are steps:
1. Create a database called `socialrepo-test` in PGAdmin.
1. Run the command to create migrations:
    ```
    DATABASE_URL=postgres://postgres:[YOUR_PASSWORD]@localhost:5432/socialrepo-test npm run migrate up
    ```

After setting up the database, we try to create a test file `src/test/routes/users.test.js` to test the user number difference between before and after creating an user which should be one. Replace `YOUR_PASSWORD` by your password.
```js
const request = require('supertest');
const buildApp = require('../../app');
const UserRepo = require('../../repos/user-repo');
const pool = require('../../pool');

beforeAll(() => {
  return pool.connect({
    host: 'localhost',
    port: 5432,
    database: 'socialrepo-test',
    user: 'postgres',
    password: 'YOUR_PASSWORD'
  })
});

afterAll(() => {
  return pool.close();
});

it('create an user', async () => {
  const startingCount = await UserRepo.count();

  await request(buildApp())
    .post('/users')
    .send({ username: 'testuser', bio: 'test bio' })
    .expect(200);

  const finishCount = await UserRepo.count();
  expect(finishCount - startingCount).toEqual(1);
});
```

Then we copy it twice and rename them as `users-two.test.js` and `users-three.test.js` under same folder. But when we try to run the command to test. You will find we got errors.
```
npm run test
```

That's because Jest runs all files in parallel, see the following diagram.
<br/><img src="./images/parallel.png" width="700"/><br/>

### Isolated Schemas
So the solution is that each test file gets its own *schema*.
- Schemas are like folders to organize things in a database.
- Every database gets a default schema called `public`.
- Each schema can have its own separate copy of a table.

<br/><img src="./images/schemas.png" width="700"/><br/>

Create a schema `test` using following command in your PGAdmin.
```sql
CREATE SCHEMA test;
```

We can operate some SQL commands in this schema just adding schema before the table like so:
```sql
-- create an users table in test schema
CREATE TABLE test.users (
  id SERIAL PRIMARY KEY,
  username VARCHAR
);

-- insert an user in users table in test schema
INSERT INTO test.users (username)
VALUES ('alex');

-- query all users in test schema
SELECT * FROM test.users;
```

If we don't specify the schema, Postgres will choose `public` schema as default. But we can customize which schema gets access if we don't specifically designate it when we write out our statement.

Postgres uses a setting internally called `search_path`. It controls which schema Postgres is going to access by default if we don't specifically designate it inside of our query. Type the command to check `search_path`.
```sql
SHOW search_path;
```

You should see the result like:
<br/><img src="./images/search-path.png" width="150"/><br/>

Whenever you connect to your Postgres instance, you are connecting as a specific user, in our case is `postgres`. `"$user"` means that if we have a schema inside of our database with the exact same name as the user that we are logged in with, then we should try to access that schema first.

So `"$user", public` means Postgres first tries a schema with the same name as the current user, then fall back to `public` schema.

Try to update `search_path` and do some tests.
```sql
SET search_path TO test, public;

-- recover default setting
SET search_path TO "$user", public;
```

So we can make use of the current user name to create a schema whose name is same with current user. So that this schema is a default. See the detail steps before testing as follows (refer to [context.js](./social-repo/src/test/context.js)):
1. Randomly generating a role name to connect to Postgres as.
1. Connect to Postgres as usual.
1. Create a new role.
1. Create a schema with the same name.
1. Disconnect entirely from Postgres.
1. Run our migrations in the new schema.
1. Connect to Postgres as the newly created role.

Notice that while we create a new role, it's safe to use `CREATE ROLE ${roleName} WITH LOGIN PASSWORD ${roleName};` directly, because this is a test, user cannot provide any string to us in theory. But it's still a good practice to prevent SQL injection. But as we mentioned before, it's not possible to use `pg` on identifier, see this [page](https://node-postgres.com/features/queries). We have to make use of [pg-format](https://www.npmjs.com/package/pg-format) instead:
```js
const format = require('pg-format');

// create a new role
await pool.query(
  format('CREATE ROLE %I WITH LOGIN PASSWORD %L;', roleName, roleName)
);
```

After testing we have to remove that role and schema using following steps:
1. Disconnect from Postgres.
1. Reconnect as root user.
1. Delete the role and schema we created (using drop cascade).
1. Disconnect.

For each test case, we can reset data before running tests using following method:
```js
async reset() {
  return pool.query(`DELETE FROM users;`);
}
```

Now let's open [context.js](./social-repo/src/test/context.js) and search for string `YOUR_PASSWORD` and replace it by your password. Then run the following command, you should not see any errors.
```js
npm run test
```
