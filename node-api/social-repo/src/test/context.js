const { randomBytes } = require('crypto');
const format = require('pg-format');
const { default: migrate } = require('node-pg-migrate');
const pool = require('../pool');

const DEFAULT_OPTS = {
  host: 'localhost',
  port: 5432,
  database: 'socialrepo-test',
  user: 'postgres',
  password: 'YOUR_PASSWORD'
}

class Context {
  static async build() {
    // randomly generating a role name to connect to Postgres as
    const roleName = 'a' + randomBytes(4).toString('hex');

    // connect to Postgres as usual
    await pool.connect(DEFAULT_OPTS);

    // create a new role
    await pool.query(
      format('CREATE ROLE %I WITH LOGIN PASSWORD %L;', roleName, roleName)
    );

    // create a schema with the same name
    await pool.query(
      format('CREATE SCHEMA %I AUTHORIZATION %I;', roleName, roleName)
    );

    // disconnect entirely from Postgres
    await pool.close();

    // run our migrations in the new schema
    await migrate({
      schema: roleName,
      direction: 'up',
      log: () => {},
      noLock: true,
      dir: 'migrations',
      databaseUrl: {
        host: 'localhost',
        port: 5432,
        database: 'socialrepo-test',
        user: roleName,
        password: roleName
      }
    });

    // connect to Postgres as the newly created role
    await pool.connect({
      host: 'localhost',
        port: 5432,
        database: 'socialrepo-test',
        user: roleName,
        password: roleName
    });

    return new Context(roleName);
  }

  constructor(roleName) {
    this.roleName = roleName;
  }

  async reset() {
    return pool.query(`DELETE FROM users;`);
  }

  async close() {
    // disconnect from Postgres
    await pool.close();

    // reconnect as root user
    await pool.connect(DEFAULT_OPTS);

    // delete the role and schema we created
    await pool.query(
      format('DROP SCHEMA %I CASCADE;', this.roleName)
    );
    await pool.query(
      format('DROP ROLE %I;', this.roleName)
    );

    // disconnect
    await pool.close();
  }
}

module.exports = Context;
