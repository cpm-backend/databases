-- Q1
SELECT paid, COUNT(*) AS count FROM orders GROUP BY paid;

-- Q2
select first_name, last_name, paid
FROM users
JOIN orders ON orders.user_id = users.id;
