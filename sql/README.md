# SQL
This tutorial is based on PostgreSQL database.

## Installation
To avoid installation issues, you can visit https://pg-sql.com/ provided by Stephen Grider and type query to learn SQL directly.

### Install PostgreSQL
To install Postgres on MacOS, you can visit https://postgresapp.com/ to download an app. Another way is to use *Homebrew* to install it, see [here](https://formulae.brew.sh/formula/postgresql).

The following content is to install Postgres using docker:
1. Download and install Docker, see [here](https://docs.docker.com/desktop/mac/install/).
1. Pull the Postgres image and start an instance, check more setting in official [document](https://hub.docker.com/_/postgres).
    ```sh
    docker run --name mydb -p 5432:5432 -e POSTGRES_PASSWORD=[YOUR_PASSWORD] -d postgres

    # To create a specific default user
    docker run --name mydb -p 5432:5432 -e POSTGRES_USER=[YOUR_USER_NAME] -e POSTGRES_PASSWORD=[YOUR_PASSWORD] -d postgres
    ```
1. Get into the instance.
    ```
    docker exec -it mydb bash
    ```
1. Login as a default super user `postgres`, if you have a specific default user, then replace `postgres` by your user name in following command.
    ```
    psql -U postgres
    ```
1. Try to get all databases in PostgreSQL. To exit `psql` you can type `exit` command.
    ```
    postgres=# \l
    ```

### Install pgAdmin
After installing and starting Postgres, let's install *pgAdmin* service which is a tool to manage and interact with Postgres using Docker. To install app version, visit pgAdmin download [page](https://www.pgadmin.org/download/).

1. Pull the pgAdmin image and start an instance.
    ```
    docker run --name mypgadmin -p 80:80 -e 'PGADMIN_DEFAULT_EMAIL=[YOUR_DEFAULT_EMAIL]' -e 'PGADMIN_DEFAULT_PASSWORD=[YOUR_PASSWORD]' -d dpage/pgadmin4
    ```
1. Go to your browser and visit http://localhost, you should see a login page. Please type the email and password you set up in previous step.
1. Click **Add New Server**.
    <br/><img src="./images/pgadmin-add-server.png" width="600"/><br/>
1. Type *localhost* for **Name** field in **General** tab.
1. Change tab to **Connection**, now we have to query our host name which is used by Postgres server. To get address ID, go back to terminal and type following command to get **IPAddress** info:
    ```
    docker inspect mydb -f "{{json .NetworkSettings.Networks}}"
    ```
1. Type the **IPAddress** you got in previous step for **Host name/address** field and *postgres* for **Username**. and type your password. Click **Save**.
    <br/><img src="./images/pgadmin-register.png" width="300"/><br/>

## Basic Operations
This section is about basic operations for SQL. You can type command in `Enter Query` block and click **Run** directly or use your pgAdmin.
    <br/><img src="./images/pg-sql.png" width="600"/><br/>

### Create Table
To create a table, use following command.
```sql
CREATE TABLE cities (
  name VARCHAR(50),
  country VARCHAR(50),
  population INTEGER,
  area INTEGER
);
```
`CREATE TABLE` is *keyword* which tells database that we want to do something and it's always written out in **capital letters**. `cities` is *identifiers* which tells database what thing we want to act on and it's always written our in **lower case letters**.

`VARCHAR(50)` and `INTEGER` are column data types. For `VARCHAR(50)`, if we put in a string longer than 50 characters, we will get an error. For `INTEGER`, the valid range is -2,147,483,647 to 2,147,483,647.

### Delete Table
To delete a table:
```sql
DROP TABLE cities;
```

### Insert Data
Insert one record:
```sql
INSERT INTO cities (name, country, population, area)
VALUES ('Tokyo', 'Japan', 38505000, 8223);
```
Insert multiple records in one command:
```sql
INSERT INTO cities (name, country, population, area)
VALUES
  ('Delhi', 'India', 28125000, 2240),
  ('Shanghai', 'China', 22125000, 4015),
  ('Sao Paulo', 'Brazil', 20935000, 3043);
```

### Retrieve Data
Retrieve all columns:
```sql
SELECT * FROM cities;
```
To retrieve some columns:
```sql
SELECT name, population FROM cities;
```

### Calculated Columns
Sometimes we need to do some math while querying the data. For example, we can calculate the population density as a new returned column called **density** as follows:
```sql
SELECT name, population / area AS density FROM cities;
```
We can also use string operations. To join two strings, we can use `||` like so:
```sql
SELECT name || ', ' || country AS location FROM cities;
```
Or you can use `CANCAT()` to join two strings:
```sql
SELECT CONCAT(name, ', ', country) AS location FROM cities;
```
Other common string operations are like `UPPER()` which gives an upper case string, `LOWER()` for lower case string and `LENGTH()` to get number of charaters in a string.

### Filtering
To filter the data, we can use keyword `WHERE`.
```sql
SELECT name, area FROM cities WHERE area > 4000;
```

We can use following comparison math operations in where clause: `=`, `!=`, `<>` (not equal which is same with `!=`), `<`, `<=`, `>`, `>=`, `IN` (is the value present in a list?), `NOT IN`, `BETWEEN` (is the value between two other values?).
```sql
SELECT name, area FROM cities
WHERE area BETWEEN 2000 AND 4000;

SELECT name, area FROM cities
WHERE name IN ('Delhi', 'Shanghai');

SELECT name, area FROM cities
WHERE area NOT IN (3043, 8223);
```

We can also use `OR` or `AND` to connect multiple conditions in our where clause like so:
```sql
SELECT name, area FROM cities
WHERE
  area NOT IN (3043, 8223)
  OR name = 'Delhi'
  OR name = 'Tokyo';
```

We can do calculation in where clause as well:
```sql
SELECT
  name,
  population / area AS population_density
FROM
  cities
WHERE
  population / area > 6000;
```

### Update Rows
To update the rows that fit where condition:
```sql
UPDATE cities
SET population = 39505000
WHERE name = 'Tokyo';
```

### Delete Rows
To delete the rows that fit where condition:
```sql
DELETE FROM cities
WHERE name = 'Tokyo';
```

## Tables
We are going to design the tables for a photo-sharing app in this section.

### Cardinality
Within data modeling, the cardinality of a join between two tables is the numerical relationship between rows of one table and rows in the other. Common cardinalities include one-to-one, one-to-many, and many-to-many. We can find the following relationship in our app:
- A user has many photos (*one-to-many* relationship).
- A photo has one user (*many-to-one* relationship).
- A photos has many comments (*one-to-many* relationship).
- A comment has one photo (*many-to-one* relationship).

There are two more relationship that are not in our app, one is called *one-to-one* relationship. For example, one capital has one country, vice versa. Another one is called *many-to-many* relationship. For example, a student has many classes and a class has many students.

### Primary Key & Foreign Key
*Primary key* uniquely identifies the record in the table, and *foreign key* identifies a record (usually in another table) that this row is associated with.
<br/><img src="./images/pk-and-fk.png" width="900"/><br/>

|Primary Keys|Foreign Keys|
|--|--|
|Each row in every table has one primary key.|Rows only have this if they belong to another record.
|No other row in the same table can have the same value.|Many rows in the same table can have the same foreign key.|
|99% of the time called "id".|Name varies, usually called something like "xyz_id".|
|Either an integer or an UUID.|Exactly equally to the primary key of the referenced row.|
|Will never change.|Will change if the relationship changes.|

To set up primary key, use keyword `PRIMARY KEY`. We can also use `SERIAL` to make database generate ID for us automatically.
```sql
CREATE TABLE users (
  id SERIAL PRIMARY KEY,
  username VARCHAR(50)
);

INSERT INTO users (username)
VALUES
  ('monahan93'),
  ('pferrer'),
  ('si93onis'),
  ('99stroman');
```

Use following context to set up foreign key in photos table and refer to users table's id column.
```sql
CREATE TABLE photos (
  id SERIAL PRIMARY KEY,
  url VARCHAR(200),
  user_id INTEGER REFERENCES users(id)
);

INSERT INTO photos (url, user_id)
VALUES
  ('http://one.jpg', 4),
  ('http://two.jpg', 1),
  ('http://25.jpg', 1),
  ('http://36.jpg', 1),
  ('http://745.jpg', 2),
  ('http://35.jpg', 3),
  ('http://256.jpg', 4);
```

When we retrieve the data from photos table, all we get is a user id which is not meaningful. So we can use `JOIN` keyword to get user info from the users table. We will deep dive `JOIN` in the following section.
```sql
SELECT url, username FROM photos
JOIN users ON users.id = photos.user_id;
```

### Foreign Key Constraints
Consider photos and users tables, there are three different cases when we try to insert a photo:
- When we insert a photo that refers to a user that exists, everything works OK.
- When we insert a photo that refers to a user that doesn't exist, we will get an error.
    ```sql
    INSERT INTO photos (url, user_id)
    VALUES ('http://error', 9999);
    ```
- If we insert a photo that isn't tied to any user, we can put in a value of "NULL" for the `user_id` column.
    ```sql
    INSERT INTO photos (url, user_id)
    VALUES ('http://error', NULL);
    ```

From the users table prospectus, if we try to delete an user which is referenced by photos table, we have following delete options:
|Delete Options|What Happened If That Row Is Still Referenced|
|--|--|
|ON DELETE RESTRICT (default)|Throw an error.
|ON DELETE NO ACTION|Throw an error.
|ON DELETE CASCADE|Delete the photos too.
|ON DELETE SET NULL|Set the `user_id` of the photo to `NULL`.
|ON DELETE SET DEFAULT|Set the `user_id` of the photo to a default value if one is provided.

We put the above settings on photos table because it depends on users table. If we want to delete the photos while deleting user, we can use create photos table like:
```sql
CREATE TABLE photos (
  id SERIAL PRIMARY KEY,
  url VARCHAR(200),
  user_id INTEGER REFERENCES users(id) ON DELETE CASCADE
);
```

To set `NULL` while deleting user:
```sql
CREATE TABLE photos (
  id SERIAL PRIMARY KEY,
  url VARCHAR(200),
  user_id INTEGER REFERENCES users(id) ON DELETE SET NULL
);
```

## Joins
For this section, please run the [script](./scripts/joins-section.sql) in advanced. If `users` and `photos` table exist, please drop them first.

**Joins** can produce values by merging together rows from different related tables. **Aggregation** looks at many rows calculates a single value, words like most, average, least are a sign that you need to use an aggregation.

### Join Examples
For each comment, show the contents of the comment and the username of the user who wrote the comment.
```sql
SELECT contents, username
FROM comments
JOIN users ON users.id = comments.user_id;
```
<br/><img src="./images/join.png" width="600"/><br/>

Table order between `FROM` and `JOIN` frequently makes a difference, but in above case it's same. Try following query, you will get same result:
```sql
SELECT contents, username
FROM users
JOIN comments ON users.id = comments.user_id;
```

We must give context if column names collide. See above diagram, if we want to get `id` from query we must give table name because there are two `id` column in join table.

```sql
-- wrong
SELECT id
FROM users
JOIN comments ON users.id = comments.user_id;

-- correct
SELECT users.id
FROM users
JOIN comments ON users.id = comments.user_id;

-- correct
SELECT comments.id, users.id AS user_id
FROM users
JOIN comments ON users.id = comments.user_id;
```

Table can be renamed using `AS` keyword as well.
```sql
SELECT comments.id, p.id AS photo_id
FROM photos AS p
JOIN comments ON p.id = comments.photo_id;
```

### Missing Data
If the foreign key column is `NULL`, we might not get the data. For example, we put a record in photos like:
```sql
INSERT INTO photos (url, user_id)
VALUES ('http://banner.jpg', NULL);
```

Because `user_id` is `NULL`, so we use a following query we cannot see this data:
```sql
SELECT url, username
FROM photos
JOIN users ON users.id = photos.user_id;
```

The root cause of the above situation is the source table of photos that doesn't match up with the row from users, then that row gets dropped from the overall results set.
<br/><img src="./images/join-missing.png" width="600"/><br/>

### Inner Join
This is a default join, you can use `JOIN` or `INNER JOIN`. The inner join is used to select all matching rows or columns in both tables.
```sql
SELECT url, username
FROM photos
JOIN users ON users.id = photos.user_id;
```
<br/><img src="./images/inner-join.png" width="600"/><br/>

### Left Outer Join
The `LEFT JOIN` is used to retrieve all records from the left table (photos) and the matched rows or columns from the right table (users). If both tables do not contain any matched rows or columns, it returns the `NULL`.
```sql
SELECT url, username
FROM photos
LEFT JOIN users ON users.id = photos.user_id;
```
<br/><img src="./images/left-outer-join.png" width="600"/><br/>

### Right Outer Join
The `RIGHT JOIN` is used to retrieve all records from the right table (users) and the matched rows or columns from the left table (photos). If both tables do not contain any matched rows or columns, it returns the `NULL`.
```sql
SELECT url, username
FROM photos
RIGHT JOIN users ON users.id = photos.user_id;
```
<br/><img src="./images/right-outer-join.png" width="600"/><br/>

### Full Join
It is a combination result set of both `LEFT JOIN` and `RIGHT JOIN`. The joined tables return all records from both the tables and if no matches are found in the table, it places `NULL`. It is also called a `FULL OUTER JOIN`.
```sql
SELECT url, username
FROM photos
FULL JOIN users ON users.id = photos.user_id;
```
<br/><img src="./images/full-join.png" width="600"/><br/>

### Where with Join
Now users can comment on photos that they posted. We can find out whom commenting on their own photos by using `WHERE`.
```sql
SELECT contents, url
FROM comments
JOIN photos ON photos.id = comments.photo_id
WHERE comments.user_id = photos.user_id;
```

### Three Way Joins
We can merge three different tables called **three way joins**
to find out the username of the person who actually posted on their own photo.
<br/><img src="./images/three-way-joins.png" width="900"/><br/>

```sql
SELECT contents, url, username
FROM comments
JOIN photos
  ON photos.id = comments.photo_id
JOIN users
  ON users.id = comments.user_id
  AND users.id = photos.user_id;
```

Notice that we often need a complicated statement in second `JOIN` statement. Because it needs to fit above two tables.

## Aggregation & Grouping
**Grouping** reduces many rows down to fewer rows, it's done by using the `GROUP BY` keyword. **Aggregation** reduces many values down to one, it's done by using *aggregate functions*.

### Group
For example, see the following query which means find the set of all unique `user_id`s and assign it to a group based on its user_id for each row.
```sql
SELECT user_id FROM comments GROUP BY user_id;
```

Notice that we can only directly select the groups column. If we select the column which doesn't appear in the GROUP BY clause or be used in an aggregate function, we will get an error. Try following query:
```sql
SELECT contents FROM comments GROUP BY user_id;
-- column "comments.contents" must appear in the GROUP BY clause or be used in an aggregate function
```

### Aggregate Functions
We can use aggregate functions to generate one value from many values. For example, `COUNT()`, `SUM()`, `AVG()`, `MAX()`, `MIN()` and so on.
```sql
SELECT SUM(id) FROM comments;
```

When we make use of an aggregate function, we cannot do a normal select next to it. Try the following command you will get an error:
```sql
SELECT SUM(id), id FROM comments;
```

We can use GROUP BY and aggregate function together. For example,
```sql
SELECT user_id, COUNT(id) AS num_comments_created
FROM comments
GROUP BY user_id;
```

In some cases, the value of column is `NULL` for some row. It's not calculated by `COUNT()`. So we usually use `COUNT(*)` to calculate the number of record instead of `COUNT(user_id)`.

### Having Keyword
`HAVING` is very similar to `WHERE` keyword. The difference between `WHERE` and `HAVING` is that `WHERE` is going to operate on filtering out some number of **rows**, whereas `HAVING` is going to filter out some number of **groups**. You are never going to see `HAVING` without a `GROUP BY`.

```sql
SELECT photo_id, COUNT(*)
FROM comments
WHERE photo_id < 3
GROUP BY photo_id
HAVING COUNT(*) > 2;
```

### Practice
Reset your database, and run the [script](./scripts/large-dataset.sql) to insert new data for the following practice.

The dataset schema is like the following diagram:
<br/><img src="./images/large-dataset.png" width="900"/><br/>

Try to write a query for the each following questions:

1. Print the number of paid and unpaid orders. The result should look like:
    ```
    +-------+-------+
    | paid  | count |
    +-------+-------+
    | true  | 4     |
    +-------+-------+
    | false | 2     |
    +-------+-------+
    ```
1. Join together the users and orders table. Print the first_name and last_name of each user, then whether or not they have paid for their order.

Check this [script](./scripts/large-dataset-ans.sql) to see the answer.

## Sorting
Run the [script](./scripts/large-dataset.sql) (same with above [Practice](./README.md#practice) section) to learn the sorting skill in this section.

We can use `ORDER BY` to sort the result. The default order is increasing.
```sql
-- sort in ascending order (default ASC)
SELECT * FROM products ORDER BY price;

-- sort in ascending order
SELECT * FROM products ORDER BY price ASC;

-- sort in descending order
SELECT * FROM products ORDER BY price ASC;
```

We can also sort name column, the default sort strategy is alphabetical order.
```sql
SELECT * FROM products ORDER BY name;

-- sort in inverse of alphabetical order
SELECT * FROM products ORDER BY name DESC;
```

We can not only sort a column, for many records have same value in first column, we can then apply a second ordering rule.
```sql
SELECT * FROM products ORDER BY price, weight;
```

### Offset & Limit
Offset means that skips some rows of the result set. Limit means that it only gives some row of the result set.
```sql
-- skip first 40 rows
SELECT * FROM users OFFSET 40;

-- only give the first 2 rows
SELECT * FROM users LIMIT 2;

-- use OFFSET nad LIMIT together
SELECT * FROM users LIMIT 2 OFFSET 3;
```

When we try to get a lot of data, we will use `OFFSET` and `LIMIT` together, it's called pagination. For example, we can only get 20 rows per query, so we can use `LIMIT 20 OFFSET 0` for the first query. Then we want to get the next 20 rows by using `LIMIT 20 OFFSET 20`.

### Unique Value
We can use `DISTINCT` keyword to find out all unique values. You can use `GROUP BY` in place of `DISTINCT`, but you cannot use `DISTINCT` in place of GROUP BY. Because only `GROUP BY` can use aggregate functions to take a look at values inside of each of those different groups.
```sql
SELECT DISTINCT department
FROM products;

SELECT DISTINCT department, name
FROM products;
```

It's very useful when we want to find out the number of all unique values.
```sql
SELECT COUNT(DISTINCT department)
FROM products;

-- you are NOT allowed to count 2 columns
SELECT COUNT(DISTINCT department, name)
FROM products;
```

### Other Utility Operators
We can use some utility operators in Postgres like `GREATEST`, `LEAST`, and `CASE`. See the following examples:

```sql
SELECT name, weight, GREATEST(30, 2 * weight)
FROM products;

SELECT name, price, LEAST(price * 0.5, 400)
FROM products;
```

We can use `CASE` clause in SQL as switch statement.
```sql
SELECT
  name,
  price,
  CASE
    WHEN price > 600 THEN 'high'
    WHEN price > 300 THEN 'medium'
    ELSE 'cheap'
  END
FROM products;
```

## Union
Union is joining multiple query together in one query, see following query:
```sql
(
  SELECT *
  FROM products
  ORDER BY price DESC
  LIMIT 4
)
UNION
(
  SELECT *
  FROM products
  ORDER BY price / weight DESC
  LIMIT 4
);
```

Now we can find that there are only seven rows in the result instead of eight. That's because `UNION` keyword sees an identical row in both lists then it will removed that duplicate. If we don't want to remove that duplicate, we can use `UNION ALL` to show all data.
```sql
(
  SELECT *
  FROM products
  ORDER BY price DESC
  LIMIT 4
)
UNION ALL
(
  SELECT *
  FROM products
  ORDER BY price / weight DESC
  LIMIT 4
);
```

It's not required to use parentheses, but in above case we should use it because it's not clear for when to apply `ORDER BY` and `LIMIT` keyword if we don't use parentheses. But we can skip parentheses in the following query without getting any issue:
```sql
SELECT * FROM products
UNION
SELECT * FROM products
```

We are only allowed to use the `UNION` keyword between the results of two queries where the result have the same columns.

### Intersect
`INTERSECT` keyword is used to find the rows common in the results of two queries. It will remove duplicate after combining.
```sql
(
  SELECT *
  FROM products
  ORDER BY price DESC
  LIMIT 4
)
INTERSECT
(
  SELECT *
  FROM products
  ORDER BY price / weight DESC
  LIMIT 4
);
```

`INTERSECT ALL` is similar with `INTERSECT`, the only difference is it doesn't remove duplicate.

Notice that it's only for more than once which means if only a same row in two tables, `INTERSECT ALL` is same with `INTERSECT`.  But if this row in table A twice and in table B once, then the result will show up twice.

### Except
`EXCEPT` is used to find the rows that are present in first query but not in second query. It will remove duplicate after combining. `EXCEPT ALL` is same with `EXCEPT`, but it won't remove duplicate.

## Subqueries
Run the [script](./scripts/large-dataset.sql) (same with above [Sorting](./README.md#sorting) section) to learn the subqueries in this section. Subquery means combine two specific queries down into one. For example, use following query to list the name and price of all products that are more expensive than all products in the *Toys* department:
```sql
SELECT name, price
FROM products
WHERE price > (
  SELECT MAX(price) FROM products WHERE department = 'Toys'
);
```

The query put in a set of parentheses is a subquery. Subqueries can be used as:
- A source of **a** value.
- A source of row**s**.
- A source of **a** column.

Notice that the different query returns different the shape of a query result, and we should use the following queries as subqueries corresponding to above different places:

|Query|Result Shape|
|--|--|
|`SELECT * FROM orders`|Many rows, many columns|
|`SELECT id FROM orders`|Many rows, one column|
|`SELECT COUNT(*) FROM orders`|One rows, one column (single value)|

### Subqueries in `SELECT`
In select statement, we need to provide a single value. For example,
```sql
-- (SELECT MAX(price) FROM products) returns a single value
SELECT name, price, (
  SELECT MAX(price) FROM products
)
FROM products
WHERE price > 867;

-- (SELECT price FROM products WHERE id = 3) returns a single value
SELECT name, price, (
  SELECT price FROM products WHERE id = 3
) AS id_3_price
FROM products
WHERE price > 867;
```

### Subqueries in `FROM`
We can put any query in `FROM` clause so long as the outer selects/wheres/etc are compatible. Subquery in FROM returns a table, compatibility means that table can be applied to outer other SQL clause. Notice that subquery in `FROM` **MUST** have an alias applied to it.
```sql
SELECT name, price_weight_ratio
FROM (
  SELECT name, price / weight AS price_weight_ratio FROM products
) AS p
WHERE price_weight_ratio > 5;

-- one single value in FROM is still valid
SELECT *
FROM (
  SELECT MAX(price) FROM products
) AS p;
```

For example, find the average number of orders of all users:
```sql
SELECT AVG(order_count)
FROM (
  SELECT user_id, COUNT(*) AS order_count
  FROM orders
  GROUP BY user_id
) AS p;
```

### Subqueries in `JOIN`
We can put any query in `JOIN` clause that returns data compatible with `ON` clause.
```sql
SELECT first_name
FROM users
JOIN (
  SELECT user_id FROM orders WHERE product_id = 3
) AS o
ON o.user_id = users.id;
```

### Subqueries in `WHERE`
Using subqueries in `WHERE` is more common and useful comparing to `FROM` or `JOIN`. See the following example, we show the id of orders that involve a product with a price/weight ratio greater than 50.
```sql
SELECT id
FROM orders
WHERE product_id IN (
  SELECT id FROM products WHERE price / weight > 50
);
```

Show the name of all products with a price greater than the average product price.
```sql
SELECT name
FROM products
WHERE price > (
  SELECT AVG(price) FROM products
);
```

See the following table to understand what operator we are going to use with the different type of data coming back.

|Operator in `WHERE`|Structure of Data That Subquery Must Return
|--|--|
|`>`|single value|
|`>=`|single value|
|`<`|single value|
|`<=`|single value|
|`=`|single value|
|`<>`|single value|
|`!=`|single value|
|`IN`|single column|
|`NOT IN`|single column|
|`> ALL/SOME/ANY`|single column|
|`< ALL/SOME/ANY`|single column|
|`>= ALL/SOME/ANY`|single column|
|`<= ALL/SOME/ANY`|single column|
|`= ALL/SOME/ANY`|single column|
|`<> ALL/SOME/ANY`|single column|

Show the name of all products that are not in the same department as products with a price less than 100.
```sql
SELECT name
FROM products
WHERE department NOT IN (
  SELECT department FROM products WHERE price < 100
);
```

Show the name, department, and price of products that are more expensive than all products in the *Industrial* department. We use a new keyword `ALL` in this case.
```sql
-- use ALL keyword
SELECT name, department, price
FROM products
WHERE price > ALL (
  SELECT price FROM products WHERE department = 'Industrial'
);

-- we can also use MAX() in this case
SELECT name, department, price
FROM products
WHERE price > (
  SELECT MAX(price) FROM products WHERE department = 'Industrial'
);
```

We can use `SOME` or `ANY` (totally same in SQL) to make sure at least one value is matched to the condition. For example, show the name of products that are more expensive than at least one product in the *Industrial* department.
```sql
-- use SOME keyword
SELECT name, department, price
FROM products
WHERE price > SOME (
  SELECT price FROM products WHERE department = 'Industrial'
);

-- we can also use MIN() in this case
SELECT name, department, price
FROM products
WHERE price > (
  SELECT MIN(price) FROM products WHERE department = 'Industrial'
);
```

### Correlated Subqueries
We can use alias to refer to a table and use this alias everywhere in query, it's like nested loop. For example, show the name, department, and price of the most expensive product in each department.
```sql
SELECT name, department, price
FROM products AS p1
WHERE p1.price = (
  SELECT MAX(price)
  FROM products AS p2
  WHERE p2.department = p1.department
);

-- it's like the following structure:
--
-- for p1 in products:
--   max = MIN_VALUE
--   for p2 in products:
--     if p1.department == p2.department:
--       max = max(max, p2.price)
--   if p1.price == max:
--     print max
```

Another example: try print the number of orders for each product without using a `JOIN` or a `GROUP BY`.
```sql
SELECT p1.name, (
  SELECT COUNT(*)
  FROM orders AS o1
  WHERE o1.product_id = p1.id
) 
FROM products AS p1;
```

### `SELECT` Subqueries without `FROM`
We can put a subquery after `SELECT` only as long as that subquery only returns **a single value**. It's useful when you want to calculate.
```sql
-- calculation
SELECT (
  SELECT MAX(price) FROM products 
) / (
  SELECT AVG(price) FROM products
);

-- list 2 results
SELECT (
  SELECT MAX(price) FROM products
),(
  SELECT AVG(price) FROM products
);
```

## PostgreSQL Datatypes
There are many **categories** of datatypes in Postgres, see the following diagram. We only focus on *Numbers*, *Date/Time*, *Character* and *Boolean* in this section.
<br/><img src="./images/datatypes.png" width="450"/><br/>

### Numeric Types
There are many subcategories in the numeric types, the range of them see the following diagram.
<br/><img src="./images/numeric.png" width="750"/><br/>

Use the following quick rules to find the suitable numeric type for the data.
<br/><img src="./images/numeric-rule.png" width="750"/><br/>

For the column needs to store a number with a decimal and **the data needs to be very accurate**, we should mark it as `numeric`. For example, bank balance, grams of gold, scientific calculations.

For the column needs to store a number with a decimal and **the decimal doesn't make a bug difference**, we should mark it as `double precision`. For example, kilograms of trash in a landfill, liters of water in a lake, air pressure in a tire.

Let's go to your localhost `postgres` database and right click on it, then click **Query Tool**. Try to type following command to see how Postgres handles the different numeric types.
```sql
-- integer
SELECT 2 + 2;

-- numeric
SELECT (2.0);

-- force to treat it as integer
SELECT (2.0::INTEGER);

-- get error with message "smallint out of range"
SELECT (99999::SMALLINT);

-- get 1.001358e-05
-- becuase REAL is float and not precise
SELECT (1.99999::REAL - 1.99998::REAL);

-- get 0.00001
-- precise answer
SELECT (1.99999::NUMERIC - 1.99998::NUMERIC);
```

### Character Types
There are some different types of character:

|Character Types|Description|
|--|--|
|`CHAR(n)`|Store some characters, length will always be `n` even if Postgres has to insert spaces.|
|`VARCHAR`|Store any length of string.|
|`VARCHAR(n)`|Store a string up to `n` characters, automatically remove extra characters.|
|`TEXT`|Store any length of string (same with `VARCHAR` behind the scene).|

Notice that **there is no performance difference between these different character types**, which is kind of unlike many other types of databases.

The only reason to put the limit on `VARCHAR` or `CHAR` it's just doing some validation for you to make sure that you don't accidentally input some string.

### Boolean Types
There are three different types of boolean:

|Boolean Types|Examples|
|--|--|
|`TRUE`|true, 'yes', 'on', 1, 't', 'y'|
|`FALSE`|false, 'no', 'off', 0, 'f', 'n'|
|`NULL`|null|

```sql
-- TRUE
SELECT ('yes'::BOOLEAN);

-- FALSE
SELECT (0::BOOLEAN);

-- NULL
SELECT (null::BOOLEAN);
```

### Date/Time Types
There are several date/time types in Postgres.
|Date/Time Types |Format Examples|
|--|--|
|`DATE`|'1980-11-20', 'Nov-20-1980', '20-Nov-1980', '1980-November-20', 'November 20, 1980' etc|
|`TIME` (same with `TIME WITHOUT TIME ZONE`)|'01:23 AM', '20:34' etc|
|`TIME WITH TIME ZONE`)|'01:23 AM EST', '05:23 PM UTC' etc|
|`TIMESTAMP` (same with `TIMESTAMP WITHOUT TIME ZONE`)|'1980-11-20 18:23:00' etc|
|`TIMESTAMP WITH TIME ZONE`|'1980-11-20 18:23:00-07' etc|

```sql
-- date: 1980-11-20
SELECT ('1980 November 20'::DATE);

-- time without time zone: 13:23:23
SELECT ('01:23:23 PM'::TIME);

-- TIME with time zone: 13:23:00+00:00
SELECT ('01:23 PM z'::TIME WITH TIME ZONE);

-- timestamp without time zone: 1980-11-20 01:23:00
SELECT ('Nov-20-1980 1:23 AM'::TIMESTAMP);

-- timestamp with time zone: 1980-11-20 09:23:00+00
SELECT ('Nov-20-1980 1:23 AM PST'::TIMESTAMP WITH TIME ZONE);
```

To calculate the duration of time, we can use `INTERVAL`. We can use `D/day`, `M`, `S` to represent day, minute and second. See the following examples:
```sql
-- interval: 20:30:45
SELECT ('1 D 20 H 30 M 45 S'::INTERVAL) - ('1 D'::INTERVAL);

-- timestamp with time zone: 1980-11-16 06:23:00+00
SELECT
  ('NOV-20-1980 1:23 AM EST'::TIMESTAMP WITH TIME ZONE)
  -
  ('4 D'::INTERVAL);

-- interval: 9 days 16:40:00
SELECT
  ('NOV-20-1980 1:23 AM EST'::TIMESTAMP WITH TIME ZONE)
  -
  ('NOV-10-1980 5:43 AM PST'::TIMESTAMP WITH TIME ZONE)
```

## Validation & Constraints
Please refer to [Installation](./README.md#installation) section to set up PostgreSQL on your local machine by using Docker. Then visit http://localhost and right click on **Databases**, select **Create**, **Database...** and type the name of the database `validation` and click **Save**.
<br/><img src="./images/validation.png" width="300"/><br/>

Right click on `validation` database and select **Query Tool** then type following command and click **Execute** button. Notice that now we only apply the query on this database.
<br/><img src="./images/create-table.png" width="750"/><br/>

```sql
CREATE TABLE products (
  id SERIAL PRIMARY KEY,
  name VARCHAR(40),
  department VARCHAR(40),
  price INTEGER,
  weight INTEGER
);
```

We can find out the *products* table under **Schemas** -> **public** -> **Tables**, then right click on *products* and select **View/Edit Data** -> **All Rows** to view all rows.

Now we insert a normal product into this table.
```sql
INSERT INTO products (name, department, price, weight)
VALUES ('Shirt', 'Clothes', 20, 1);
```

### Row-level Validation
The problem is we can insert a product without price without getting any error like so:
```sql
INSERT INTO products (name, department, weight)
VALUES ('Shirt', 'Clothes', 3);
```

To avoid users buy something that doesn't need money, we should add a validation to check the value before it's stored. There are two ways we can apply a null constraint to a table.

- When creating the table...
    ```sql
    CREATE TABLE products (
      id SERIAL PRIMARY KEY,
      name VARCHAR(40),
      department VARCHAR(40),
      price INTEGER NOT NULL,
      weight INTEGER
    );
    ```
- After the table was created...
    ```sql
    ALTER TABLE products
    ALTER COLUMN price
    SET NOT NULL;
    ```

Of course, we don't want to delete whole table and create it again so we should choose second way in this case. However, we will get an error while executing the second command because the column `price` contains null values now.

There are two options to handle this:
- Find all the rows inside this table that have a price of `NULL` and delete them.
- Find all the different rows inside this table with a price of `NULL` and update it to some other value.
    ```sql
    UPDATE products
    SET price = 9999
    WHERE price IS NULL;
    ```

After updating the column definition, let's try the following SQL again, we will get an error now.
```sql
INSERT INTO products (name, department, weight)
VALUES ('Shirt', 'Clothes', 3);
```

### Default Value
There are two ways to set up default value.
- When creating the table...
    ```sql
    CREATE TABLE products (
      id SERIAL PRIMARY KEY,
      name VARCHAR(50) NOT NULL,
      department VARCHAR(50) NOT NULL,
      price INTEGER DEFAULT 999,
      weight INTEGER
    );
    ```
- After the table was created...
    ```sql
    ALTER TABLE products
    ALTER COLUMN price
    SET DEFAULT 999;
    ```

Let's try to insert data without price, it will use default value without any error.
```sql
INSERT INTO products (name, department, weight)
VALUES ('Gloves', 'Tools', 1);
```

### Unique Constraint
If we don't want two items using same name, we can use `UNIQUE` keyword.
- When creating the table...
    ```sql
    CREATE TABLE products (
      id SERIAL PRIMARY KEY,
      name VARCHAR(50) UNIQUE,
      department VARCHAR(50),
      price INTEGER,
      weight INTEGER
    );
    ```
- After the table was created...
    ```sql
    ALTER TABLE products
    ADD UNIQUE(name);
    ```
  
Now we use second one, but we will get an error because now we have two items named *Shirt*. There are several ways to handle this, but we can edit the items directly in the query result. For example, we can replace one *Shirt* by *Red Shirt*.
<br/><img src="./images/edit-result.png" width="600"/><br/>

Remember that you have to click **Save Data Changes** button to save the edited value.
<br/><img src="./images/save-data.png" width="450"/><br/>

Now we can update the column definition, it will not allow data with same name now.

To see all constraints on table, you can find it in **Schemas** -> **public** -> **Tables** -> *products* -> **Constraints** like so:
<br/><img src="./images/constraints.png" width="200"/><br/>

Use the following command to delete constraint *products_name_key*:
```sql
ALTER TABLE products
DROP CONSTRAINT products_name_key;
```

We can put constraint on multi column.
```sql
ALTER TABLE products
ADD UNIQUE (name, department);

-- success
INSERT INTO products (name, department, price, weight)
VALUES ('Shirt', 'Housewares', 24, 1);

-- get wrong
INSERT INTO products (name, department, price, weight)
VALUES ('Shirt', 'Clothes', 24, 1);
```

### Validation Check
We don't want the price is less than 0, we can use `CHECK` keyword. The condition in CHECK must be true in order for us to be allowed to insert a new row.
- When creating the table...
    ```sql
    CREATE TABLE products (
      id SERIAL PRIMARY KEY,
      name VARCHAR(50) UNIQUE,
      department VARCHAR(50),
      price INTEGER CHECK (price > 0),
      weight INTEGER
    );
    ```
- After the table was created...
    ```sql
    ALTER TABLE products
    ADD CHECK (price > 0);
    ```

Notice that a check can only work on the row we are adding/updating, we cannot use subqeury in the check.

Sometimes we have to set up a check for multi columns. For example, in the following table we can set up a check to avoid delivery time earlier than created time.
```sql
CREATE TABLE orders (
  id SERIAL PRIMARY KEY,
  name VARCHAR(40) NOT NULL,
  created_at TIMESTAMP NOT NULL,
  est_delivery TIMESTAMP NOT NULL,
  CHECK (created_at < est_delivery)
);
```

### Validation Timing
In general, we will apply our database in some application which is running on web server. So the question is should we add validation on web server or on database?

There is no absolutely answer, but we can put the bulk of the validation at the web server level, and only put some very critical validation at at the database level like price should greater than zero.
