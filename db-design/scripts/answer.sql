-- 1. Select the 3 users with the highest IDs from the users table
SELECT *
FROM users
ORDER BY id DESC
LIMIT 3;

-- 2. Join the users and posts table. Show the username of user ID 200 and the captions of all posts they have created
SELECT username, caption
FROM users
JOIN posts ON posts.user_id = users.id
WHERE users.id = 200;

-- 3. Show each username and the number of *like*s that they have created
SELECT username, COUNT(*)
FROM users 
JOIN likes ON likes.user_id = users.id
GROUP BY username;
