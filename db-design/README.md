# Database Design
This tutorial is based on PostgreSQL database. It's recommended to study the [SQL](../sql/) first.

## Database Design for Instagram
We will build out a more serious version of an Instagram schema in the following tutorials.
<br/><img src="./images/instagram.png" width="1000"/><br/>

When we work with many tables, it's nice to document the database structure somehow. We can use a *schema designer* to help guide our design. There are many SQL schema designers:
- [dbdiagram.io](https://dbdiagram.io/)
- [drawsql.app](https://drawsql.app/)
- [sqldbm.com](https://sqldbm.com/)
- [quickdatabasediagrams.com](https://www.quickdatabasediagrams.com/)
- [ondras.zarovi.cz/sql/demo](https://ondras.zarovi.cz/sql/demo/)

### Schema Designer
Visit https://ondras.zarovi.cz/sql/demo/ and you can try to play around with it.
<br/><img src="./images/ondras-demo.png" width="450"/><br/>

For the [dbdiagram.io](https://dbdiagram.io/), it's a config-based schema designer. Click **Go To App** on top then copy the [file](./scripts/simple.dsl) in the left panel, you should see the following result.
<br/><img src="./images/dbdiagram.png" width="600"/><br/>

Notice that the language used by *dbdiagram* is DBML instead of SQL, but it's still very straightforward. You can get more details [here](https://www.dbml.org/home/#intro).

In the following sections, we are going to use *dbdiagram* to design our database. You can copy the script in [instagram.dbml](./scripts/instagram.dbml) and paste it on the *dbdiagram* to review our final design.
<br/><img src="./images/instagram-dbdiagram.png" width="750"/><br/>

### *Like* System Design
Now think about the *like* system for the Instagram, the requirements are as follows:
- Each user can like a specific post a single time.
- A user should be able to *unlike* a post.
- Need to be able to figure out how many users like a post.
- Need to be able to list which users like a post.
- Something besides a post might need to be like, e.g. comments.
- We might think about *dislike* or other kinds of reactions for the future extension.

Users and posts are *many-to-many* relationship, so we can design the database like the following diagram:
<br/><img src="./images/like.png" width="750"/><br/>

Notice that we don't allow a user likes a post twice, so we need to add an unique constraint like so:
```sql
ALTER TABLE reactions
ADD UNIQUE (user_id, posts_id);
```

Now there is still an issue here, if we want this table can also record who likes comments, how should we do? There are 3 different solutions can handle this.

- Polymorphic association: We can store the `liked_type` to tell this *like* is for posts or comments. But the problem is that `liked_id` could represent `post.id` or `comment.id` based on `liked_type`, which means that we cannot use foreign key on this column to prevent data inconsistency.
    <br/><img src="./images/polymorphic-association.png" width="600"/><br/>
- Polymorphic association improvement: We can use `post_id` and `comment_id` column to apply foreign keys, and set up a check for them to make sure only one value is valid and another is `NULL`. The downside of this solution is someday if we have to record a lot of different types in this table, there are too many columns in it.
    <br/><img src="./images/polymorphic-association-improvement.png" width="600"/><br/>
- Simplest alternative: we can use two tables to record posts likes and comments likes. The downside of this method is we have to create a table for new different type, but it's also good because we have a good isolation and don't need to create validation rules.
    <br/><img src="./images/simplest.png" width="600"/><br/>

We are going to use second solution in our design.

### *Mention* System Design
When we create a post, we can tag people in our photo and add location for this post. We will focus on tagging people in this section. Notice that a tag is added to a specific spot in a photo.

A quick design is like the following diagram.
<br/><img src="./images/mention.png" width="750"/><br/>
<br/><img src="./images/mention-xy.png" width="450"/><br/>

But there's a small issue there, we notice that we can tag people in our posts, how do we put those different things in tags table or should we do that?
<br/><img src="./images/post-tag.png" width="450"/><br/>

It's a tricky question, in fact, highlighted text doesn't necessarily mean that we need to store something in the database, app level could in charge of it.

So it's a question of whether or not that mention needs to trigger some other kind of behavior or if we want to reduce some metrics or somehow link to the back to this thing. We should start to think about the behavior like:
- Do we need to show a list of posts a user was mentioned in?
- Do we need to show a list of the most often mentioned users?
- Do we need to notify a user when they've been mentioned?

So if the above features we want to have, which implies that we need to store a list of people who get mentioned inside of a particular post.

There are two possible solutions if we want to store the data.
- One table: Using `NULL` for `x` and `y` column if that tag is in a caption.
- Two tables: Store two data in separate tables like `photo_tags` (with `x` and `y` column) and `caption_tags`.

So which solution is better? It depends on our use scenarios, try to think about the following questions:
- Do you expect to query for *caption tags* and *photo tags* at different rates? If yes, we should choose second solution.
- Will the meaning of a photo tag change at some point? For example, we want to add in more functionality to a photo tag. If yes, we should choose second solution.

We will go second solution in our design.

### *Hashtag* System Design
Seeing hashtags used in posts, comments, and user biographies. Again, we should think about do we expect to run a query to see what posts/comments/users contain a given hashtag?

Let's see how do we get when we try to search hashtag in real Instagram. All we get is only posts like so:

<br/><img src="./images/hashtag-search.png" width="600"/><br/>

So based on the use case, we can search for posts that contain a hashtag that implies the hashtags in a post's caption are modeled in the database. For the comments and users, we don't need to model them because we can't search for them.

<br/><img src="./images/hashtag-table.png" width="750"/><br/>

### *Follower* System Design
In database design, we don't want to store the data that can be calculated by running a query on data that exists in the database which called *derived data*. For example, we don't need to store **Posts**, **Followers** and **Following** in users table.
<br/><img src="./images/user-profile.png" width="300"/><br/>

We can design the table for followers like so, notice that we have to make leader who is followed cannot be same with follower, and one follower can only follow an user one time.
<br/><img src="./images/follower.png" width="600"/><br/>

## Database Implementation
We are going to implement our design in a new database using *PGAdmin* in this section. Let's go to your local machine and open PGAdmin to create a new database called *instagram*.
<br/><img src="./images/instagram-db.png" width="300"/><br/>

Let's create a users table first:
```sql
CREATE TABLE users (
  id SERIAL PRIMARY KEY,
  created_at TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP,
  updated_at TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP,
  username VARCHAR(30) NOT NULL,
  bio VARCHAR(400),
  avatar VARCHAR(200),
  phone VARCHAR(25),
  email VARCHAR(40),
  password VARCHAR(50),
  status VARCHAR(15),
  CHECK(COALESCE(phone, email) IS NOT NULL)
);
```

For the function `COALESCE()`, it will return the first item which is not null. We can use it to make sure at least one value is not within `phone` and `email` column.

Notice that if we want a user must provide a value for us we should use `NOT NULL`. If we just only want a value but it's optional for user input, we can use `NOT NULL DEFAULT`.

We create a posts table as follows, notice that we want to put validation rules on `lat` and `lng` column. If an user provide the location, that location must be valid. Otherwise, `NULL` is valid which means that user doesn't provide location. For the column `user_id`, we want to delete posts if that user is deleted so that we use `ON DELETE CASCADE` here.
```sql
CREATE TABLE posts (
  id SERIAL PRIMARY KEY,
  created_at TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP,
  updated_at TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP,
  url VARCHAR(200) NOT NULL,
  caption VARCHAR(240),
  lat REAL CHECK(lat IS NULL OR (lat >= -90 AND lat <= 90)),
  lng REAL CHECK(lng IS NULL OR (lng >= -180 AND lng <= 180)),
  user_id INTEGER NOT NULL REFERENCES users(id) ON DELETE CASCADE
);
```

Create the comments table:
```sql
CREATE TABLE comments (
  id SERIAL PRIMARY KEY,
  created_at TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP,
  updated_at TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP,
  contents VARCHAR(240) NOT NULL,
  user_id INTEGER NOT NULL REFERENCES users(id) ON DELETE CASCADE,
  post_id INTEGER NOT NULL REFERENCES posts(id) ON DELETE CASCADE
);
```

For the likes table, we have to make sure `post_id` and `comment_id` are only one column has value. We also don't allow an user like a post or comment twice.
```sql
CREATE TABLE likes (
  id SERIAL PRIMARY KEY,
  created_at TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP,
  user_id INTEGER NOT NULL REFERENCES users(id) ON DELETE CASCADE,
  post_id INTEGER REFERENCES posts(id) ON DELETE CASCADE,
  comment_id INTEGER REFERENCES comments(id) ON DELETE CASCADE,
  CHECK(
    COALESCE((post_id)::BOOLEAN::INTEGER, 0)
    +
    COALESCE((comment_id)::BOOLEAN::INTEGER, 0)
    = 1
  ),
  UNIQUE(user_id, post_id, comment_id)
);
```

Create two tables about tags:
```sql
CREATE TABLE photo_tags (
  id SERIAL PRIMARY KEY,
  created_at TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP,
  updated_at TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP,
  user_id INTEGER NOT NULL REFERENCES users(id) ON DELETE CASCADE,
  post_id INTEGER NOT NULL REFERENCES posts(id) ON DELETE CASCADE,
  x INTEGER NOT NULL,
  y INTEGER NOT NULL,
  UNIQUE(user_id, post_id)
);

CREATE TABLE caption_tags (
  id SERIAL PRIMARY KEY,
  created_at TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP,
  user_id INTEGER NOT NULL REFERENCES users(id) ON DELETE CASCADE,
  post_id INTEGER NOT NULL REFERENCES posts(id) ON DELETE CASCADE,
  UNIQUE(user_id, post_id)
);
```

For other tables:
```sql
CREATE TABLE hashtags (
  id SERIAL PRIMARY KEY,
  created_at TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP,
  title VARCHAR(20) NOT NULL UNIQUE
);

CREATE TABLE hashtags_posts (
  id SERIAL PRIMARY KEY,
  hashtag_id INTEGER NOT NULL REFERENCES hashtags(id) ON DELETE CASCADE,
  post_id INTEGER NOT NULL REFERENCES posts(id) ON DELETE CASCADE,
  UNIQUE(hashtag_id, post_id)
);

CREATE TABLE followers (
  id SERIAL PRIMARY KEY,
  created_at TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP,
  leader_id INTEGER NOT NULL REFERENCES users(id) ON DELETE CASCADE,
  follower_id INTEGER NOT NULL REFERENCES users(id) ON DELETE CASCADE,
  UNIQUE(leader_id, follower_id)
);
```

## Data Dumping
To practice query skill, we need to import some data in the database first. Follow the steps to dump data in the database:

1. Go to *instagram* database and right click on it, select **Restore...**.
1. Click file icon in **Filename** column and click upload button on the top, then choose the [ig.sql](./scripts/ig.sql). Remember to select **All Files** as **Format** like so:
    <br/><img src="./images/upload-file.png" width="600"/><br/>
1. Switch tabs to **Data/Objects** and **Options**, change the settings as follows:
    <br/><img src="./images/restore-1.png" width="600"/><br/>
    <br/><img src="./images/restore-2.png" width="600"/><br/>
1. Click **Restore** button to finish.

### Restoring Database
If you accidentally make a bad change to the database, this section is for teaching you how to revert to earlier state.

1. Close down all the open Query Tool windows.
1. Right click *instagram* database and select **Dashboard** tab. Make sure only one entry in the **Server activity**. If you see more than one, then click **X** to the very left hand side of that row.
    <br/><img src="./images/session.png" width="600"/><br/>
1. Right click *instagram* database again and click **Delete/Drop** to delete the whole database.
1. Recreate *instagram* database.
1. Right click on *instagram* database and select **Restore...**.
1. Click file icon in **Filename** column and click upload button on the top, then choose the [ig.sql](./scripts/ig.sql). Remember to select **All Files** as **Format**.
1. Switch tabs to **Data/Objects** and **Options**, change the settings as follows (don't turn on **Only data** for **Type of objects**):
    <br/><img src="./images/restore-3.png" width="600"/><br/>
    <br/><img src="./images/restore-4.png" width="600"/><br/>
1. Click **Restore** button to finish.

### Query Practice
Try to create a query for the following questions to practice SQL, you can find the answer in [answer.sql](./scripts/answer.sql):

1. Select the 3 users with the highest IDs from the users table.
1. Join the users and posts table. Show the username of user ID 200 and the captions of all posts they have created.
1. Show each username and the number of *like*s that they have created.
