# Database Advanced Topics

## Data Replication
Data Replication is the process of storing data in more than one site or node. It is useful in improving the availability of data. There are 3 important replication models in distributed systems:
- **Transactional replication**: Users receive full initial copies of the database and then receive updates as data changes. Data is copied in real-time from the publisher to the receiving database (subscriber) in the same order as they occur with the publisher therefore in this type of replication, **transactional consistency** is guaranteed. Transactional replication is typically used in server-to-server environments. It does not simply copy the data changes, but rather consistently and accurately replicates each change.
- **Snapshot replication**: It distributes data exactly as it appears at a specific moment in time does not monitor for updates to the data. When synchronization occurs, the entire snapshot is generated and sent to subscribers. Snapshot replication is generally used when data changes are infrequent. Snapshot replication is a good way to perform initial synchronization between the publisher and the subscriber.
- **Merge replication**: Data from two or more databases is combined into a single database. Merge replication is the most complex type of replication because it allows both publisher and subscriber to independently make changes to the database. Merge replication is typically used in server-to-client environments. It allows changes to be sent from one publisher to multiple subscribers.

## Sharding Strategies
Sharding is a method of splitting and storing a single logical dataset in multiple databases.

<img src="../screenshots/vertical-horizontal-partitions.png" alt="vertical-horizontal-partitions"/>

- *Horizontal partitioning*: The practice of separating one table's rows into multiple different tables, known as partitions.
- *Vertical partitioning*: Entire columns are separated out and put into new, distinct tables.

Using sharding for database has some benefits:
- It can help to facilitate horizontal scaling, also known as *scaling out*. Horizontal scaling is the practice of adding more machines to an existing stack. So database will not be limited in terms of storage and compute power.
- It can speed up query response times.
- Sharding can also help to make an application more reliable by mitigating the impact of outages.

Using sharding for database has some drawbacks:
- Sharding is the sheer complexity of properly implementing a sharded database architecture.
- Shards eventually become unbalanced. The database would likely need to be repaired and resharded to allow for a more even data distribution.
- Once a database has been sharded, it can be very difficult to return it to its unsharded architecture.
- Sharding isn't natively supported by every database engine. For instance, PostgreSQL does not include automatic sharding as a feature, although it is possible to manually shard a PostgreSQL database.

### Sharding Architectures
There are 3 architectures to introduce here:
- Key based sharding: It's also known as hash based sharding, using a hash value of newly written data to determine which shard the data should go to. We need to use a static column to generate hash value, this column is called shard key. This key is similar to primary key, it's static and unique. While key based sharding limits you to use a fixed hash function which can be exceedingly difficult to change later on.
<img src="../screenshots/key-based.png" alt="key-based" />

- Range based sharding: Sharding is based on ranges of a given value. This is simple one, but data might be unevenly distributed.
<img src="../screenshots/range-based.png" alt="range-based" />

- Directory based sharding: To implement directory based sharding, one must create and maintain a lookup table that uses a shard key to keep track of which shard holds which data. This is the most flexible of the sharding methods, but you need to connect to the lookup table before every query or write.
<img src="../screenshots/directory-based.png" alt="directory-based" />

### Should I Shard?
Sharding is usually only performed when dealing with very large amounts of data. Here are some scenarios:
- The amount of application data grows to exceed the storage capacity of a single database node.
- The volume of writes or reads to the database surpasses what a single node or its read replicas can handle, resulting in slowed response times or timeouts.
- The network bandwidth required by the application outpaces the bandwidth available to a single database node and any read replicas, resulting in slowed response times or timeouts.

Before sharding, we should exhaust all other options for optimizing our database. Sharding can be a great solution for those looking to scale their database horizontally. However, it also adds a great deal of complexity and creates more potential failure points for your application. Sharding may be necessary for some, but the time and resources needed to create and maintain a sharded architecture could outweigh the benefits for others.

## CAP Theorem
CAP theorem states that it is impossible for a distributed data store to simultaneously provide more than 2 out of the following 3 guarantees:
- **Consistency**: Every read receives the most recent write or an error.
- **Availability**: Every request receives a (non-error) response, without the guarantee that it contains the most recent write.
- **Partition tolerance**: The system continues to operate despite an arbitrary number of messages being dropped (or delayed) by the network between nodes.

A system that is partition tolerant can sustain any amount of network failure that doesn't result in a failure of the entire network. When dealing with modern distributed systems, partition tolerance is necessity. Hence, we have to trade between consistency and availability.

- Partition tolerance with high consistency: This condition states that all nodes see the same data at the same time. In this model, the nodes will need time to update and will not be available on the network as often.
- Partition tolerance with high availability: This condition states that every request gets a response on success/failure. Every client gets a response, regardless of the state of any individual node in the system. High availability isn't feasible when analyzing streaming data at high frequency.

## Hibernate Cache
https://www.java67.com/2017/10/difference-between-first-level-and-second-level-cache-in-Hibernate.html
