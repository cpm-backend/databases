package cpm.chuck.dao;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Version;
import lombok.Data;

@Entity
@Table(name = "department")
@Data
public class DepartmentDao {

  @Id
  private int id;

  private String name;

  @Version
  private Long version;
}
