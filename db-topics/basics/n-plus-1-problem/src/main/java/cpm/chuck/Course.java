package cpm.chuck;

import lombok.Data;

@Data
public class Course {

  private int id;
  private String name;
}
