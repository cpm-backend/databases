package cpm.chuck.repositories;

import cpm.chuck.dao.StudentDao;
import java.util.List;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface StudentRepository extends CrudRepository<StudentDao, Integer>, StudentRepositoryCustom,
    JpaSpecificationExecutor<StudentDao> {

  List<StudentDao> findAll();

  @Query("select distinct s from StudentDao s join fetch s.department")
  List<StudentDao> fetchJoinDepartment();

  @Query("select distinct s from StudentDao s join fetch s.phones")
  List<StudentDao> fetchJoinPhones();

  @Query("select distinct s from StudentDao s join fetch s.courses")
  List<StudentDao> fetchJoinCourses();

  @Query("select distinct s from StudentDao s join fetch s.department left join fetch s.courses")
  List<StudentDao> fetchJoinDepartmentAndCourses();

  @Query("select distinct s from StudentDao s join fetch s.department left join fetch s.phones")
  List<StudentDao> fetchJoinDepartmentAndPhones();
}
