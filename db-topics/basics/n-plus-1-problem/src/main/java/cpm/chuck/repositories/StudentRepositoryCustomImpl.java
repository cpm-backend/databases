package cpm.chuck.repositories;

import cpm.chuck.dao.StudentDao;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class StudentRepositoryCustomImpl implements StudentRepositoryCustom {

  @Autowired
  EntityManager em;

  @Override
  public List<StudentDao> findAllByCriteriaQuery() {
    CriteriaBuilder cb = em.getCriteriaBuilder();
    CriteriaQuery<StudentDao> query = cb.createQuery(StudentDao.class);

    Root<StudentDao> student = query.from(StudentDao.class);
    query.select(student);

    TypedQuery<StudentDao> typedQuery = em.createQuery(query);
    return typedQuery.getResultList();
  }

  @Override
  public List<StudentDao> fetchJoinDepartmentByCriteriaQuery() {
    CriteriaBuilder cb = em.getCriteriaBuilder();
    CriteriaQuery<StudentDao> query = cb.createQuery(StudentDao.class);

    Root<StudentDao> student = query.from(StudentDao.class);
    student.fetch("department");
    query.select(student);

    TypedQuery<StudentDao> typedQuery = em.createQuery(query);
    return typedQuery.getResultList();
  }

  @Override
  public List<StudentDao> fetchJoinDepartmentAndCoursesByCriteriaQuery() {
    CriteriaBuilder cb = em.getCriteriaBuilder();
    CriteriaQuery<StudentDao> query = cb.createQuery(StudentDao.class);

    Root<StudentDao> student = query.from(StudentDao.class);
    student.fetch("department");
    student.fetch("courses");
    query.select(student);

    TypedQuery<StudentDao> typedQuery = em.createQuery(query);
    return typedQuery.getResultList();
  }

}
