package cpm.chuck;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class StudentController {

  @Autowired
  private StudentService service;

  @GetMapping("/students")
  public List<Student> getStudents() {
    return service.getStudents();
  }

  @GetMapping("/fetch-join-deparment")
  public List<Student> getByFetchJoinDepartment() {
    return service.getByFetchJoinDepartment();
  }

  @GetMapping("/fetch-join-phones")
  public List<Student> getByFetchJoinPhones() {
    return service.getByFetchJoinPhones();
  }

  @GetMapping("/fetch-join-courses")
  public List<Student> getByFetchJoinCourses() {
    return service.getByFetchJoinCourses();
  }

  @GetMapping("/fetch-join-department-courses")
  public List<Student> getByFetchJoinDepartmentAndCourses() {
    return service.getByFetchJoinDepartmentAndCourses();
  }

  @GetMapping("/fetch-join-department-phones")
  public List<Student> getByFetchJoinDepartmentAndPhones() {
    return service.getByFetchJoinDepartmentAndPhones();
  }

  @GetMapping("/cq/students")
  public List<Student> getByCriteriaQuery() {
    return service.getByCriteriaQuery();
  }

  @GetMapping("/cq/fetch-join-deparment")
  public List<Student> getByCriteriaQueryAndFetchJoinDepartment() {
    return service.getByCriteriaQueryAndFetchJoinDepartment();
  }

  @GetMapping("/cq/fetch-join-department-courses")
  public List<Student> getByCriteriaQueryAndFetchJoinDepartmentAndCourses() {
    return service.getByCriteriaQueryAndFetchJoinDepartmentAndCourses();
  }

  @GetMapping("/specs/students")
  public List<Student> getBySpecificationAndName(@RequestParam("name") String name) {
    return service.getBySpecificationAndName(name);
  }

}
