package cpm.chuck;

import cpm.chuck.dao.CourseDao;
import cpm.chuck.dao.DepartmentDao;
import cpm.chuck.dao.PhoneDao;
import cpm.chuck.dao.StudentDao;
import cpm.chuck.repositories.StudentRepository;
import cpm.chuck.repositories.StudentSpecs;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class StudentService {

  @Autowired
  private StudentRepository studentRepository;

  public List<Student> getStudents() {
    List<StudentDao> daos = studentRepository.findAll();
    List<Student> students = daos.stream().map(dao -> convert(dao)).collect(Collectors.toList());
    return students;
  }

  public List<Student> getByFetchJoinDepartment() {
    List<StudentDao> daos = studentRepository.fetchJoinDepartment();
    List<Student> students = daos.stream().map(dao -> convert(dao)).collect(Collectors.toList());
    return students;
  }

  public List<Student> getByFetchJoinPhones() {
    List<StudentDao> daos = studentRepository.fetchJoinPhones();
    List<Student> students = daos.stream().map(dao -> convert(dao)).collect(Collectors.toList());
    return students;
  }

  public List<Student> getByFetchJoinCourses() {
    List<StudentDao> daos = studentRepository.fetchJoinCourses();
    List<Student> students = daos.stream().map(dao -> convert(dao)).collect(Collectors.toList());
    return students;
  }

  public List<Student> getByFetchJoinDepartmentAndCourses() {
    List<StudentDao> daos = studentRepository.fetchJoinDepartmentAndCourses();
    List<Student> students = daos.stream().map(dao -> convert(dao)).collect(Collectors.toList());
    return students;
  }

  public List<Student> getByFetchJoinDepartmentAndPhones() {
    List<StudentDao> daos = studentRepository.fetchJoinDepartmentAndPhones();
    List<Student> students = daos.stream().map(dao -> convert(dao)).collect(Collectors.toList());
    return students;
  }

  public List<Student> getByCriteriaQuery() {
    List<StudentDao> daos = studentRepository.findAllByCriteriaQuery();
    List<Student> students = daos.stream().map(dao -> convert(dao)).collect(Collectors.toList());
    return students;
  }

  public List<Student> getByCriteriaQueryAndFetchJoinDepartment() {
    List<StudentDao> daos = studentRepository.fetchJoinDepartmentByCriteriaQuery();
    List<Student> students = daos.stream().map(dao -> convert(dao)).collect(Collectors.toList());
    return students;
  }

  public List<Student> getByCriteriaQueryAndFetchJoinDepartmentAndCourses() {
    List<StudentDao> daos = studentRepository.fetchJoinDepartmentAndCoursesByCriteriaQuery();
    List<Student> students = daos.stream().map(dao -> convert(dao)).collect(Collectors.toList());
    return students;
  }

  public List<Student> getBySpecificationAndName(String name) {
    List<StudentDao> daos = studentRepository.findAll(StudentSpecs.getStudentsByName(name));
    List<Student> students = daos.stream().map(dao -> convert(dao)).collect(Collectors.toList());
    return students;
  }


  private Student convert(StudentDao dao) {
    Student student = new Student();
    student.setId(dao.getId());
    student.setName(dao.getName());
    student.setDepartment(convert(dao.getDepartment()));
    student.setCourses(dao.getCourses().stream().map(courseDao -> convert(courseDao))
        .collect(Collectors.toList()));
    student.setPhones(
        dao.getPhones().stream().map(phoneDao -> convert(phoneDao)).collect(Collectors.toList()));
    return student;
  }

  private Department convert(DepartmentDao dao) {
    Department department = new Department();
    department.setId(dao.getId());
    department.setName(dao.getName());
    return department;
  }

  private Course convert(CourseDao dao) {
    Course course = new Course();
    course.setId(dao.getId());
    course.setName(dao.getName());
    return course;
  }

  private Phone convert(PhoneDao dao) {
    Phone phone = new Phone();
    phone.setPhoneNumber(dao.getPhoneNumber());
    return phone;
  }
}
