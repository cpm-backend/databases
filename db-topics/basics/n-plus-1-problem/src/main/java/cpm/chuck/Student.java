package cpm.chuck;

import java.util.List;
import lombok.Data;

@Data
public class Student {

  private int id;
  private String name;
  private Department department;
  private List<Course> courses;
  private List<Phone> phones;
}
