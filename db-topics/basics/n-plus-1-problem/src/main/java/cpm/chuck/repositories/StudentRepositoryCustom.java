package cpm.chuck.repositories;

import cpm.chuck.dao.StudentDao;
import java.util.List;

public interface StudentRepositoryCustom {

  List<StudentDao> findAllByCriteriaQuery();

  List<StudentDao> fetchJoinDepartmentByCriteriaQuery();

  List<StudentDao> fetchJoinDepartmentAndCoursesByCriteriaQuery();
}
