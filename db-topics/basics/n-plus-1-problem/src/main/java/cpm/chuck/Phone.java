package cpm.chuck;

import lombok.Data;

@Data
public class Phone {

  private String phoneNumber;
}
