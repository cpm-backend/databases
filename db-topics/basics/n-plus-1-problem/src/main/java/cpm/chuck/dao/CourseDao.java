package cpm.chuck.dao;

import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.persistence.Version;
import lombok.Data;

@Entity
@Table(name = "course")
@Data
public class CourseDao {

  @Id
  private int id;

  private String name;

  @ManyToMany(cascade= CascadeType.ALL, mappedBy="courses")
  private List<StudentDao> students;

  @Version
  private Long version;
}
