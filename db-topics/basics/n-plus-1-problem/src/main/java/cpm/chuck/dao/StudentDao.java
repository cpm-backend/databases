package cpm.chuck.dao;

import static javax.persistence.FetchType.EAGER;
import static javax.persistence.FetchType.LAZY;

import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Version;
import lombok.Data;

@Entity
@Table(name = "student")
@Data
public class StudentDao {

  @Id
  private int id;

  private String name;

  @ManyToOne(cascade = CascadeType.ALL)
  @JoinColumn(name = "department_id")
  private DepartmentDao department;

  @ManyToMany(cascade=CascadeType.ALL)
  @JoinTable(
    name="student_course",
    joinColumns={@JoinColumn(name="student_id")},
    inverseJoinColumns={@JoinColumn(name="course_id")}
  )
  private List<CourseDao> courses;

  @OneToMany(cascade=CascadeType.ALL)
  @JoinColumn(name="student_id")
  private List<PhoneDao> phones;

  @Version
  private Long version;

}
