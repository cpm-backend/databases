package cpm.chuck.repositories;

import cpm.chuck.dao.StudentDao;
import org.springframework.data.jpa.domain.Specification;

public class StudentSpecs {

  public static Specification<StudentDao> getStudentsByName(String name) {
    return (student, query, cb) -> {
      query.distinct(true);
      student.fetch("department");
      student.fetch("courses");
      return cb.like(student.get("name"), "%" + name + "%");
    };
  }
}
