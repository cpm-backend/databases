package cpm.chuck.dao;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Version;
import lombok.Data;

@Entity
@Table(name = "phone")
@Data
public class PhoneDao {

  @Id
  private String phoneNumber;

  @Version
  private Long version;
}
