create table department(
  id int not null primary key,
  name varchar(255) not null,
  version bigint default 0 not null
);

create table course(
  id int not null primary key,
  name varchar(255) not null,
  version bigint default 0 not null
);

create table student(
  id int not null primary key,
  name varchar(255) not null,
  department_id int not null,
  version bigint default 0 not null,
  constraint fk_student_department_id foreign key(department_id) references department(id)
);

create table phone(
  phone_number varchar(255) not null primary key,
  student_id int not null,
  version bigint default 0 not null,
  constraint fk_phone_student_id foreign key(student_id) references student(id)
);

create table student_course(
  student_id int not null,
  course_id int not null,
  constraint fk_student_course_student_id foreign key(student_id) references student(id),
  constraint fk_student_course_course_id foreign key(course_id) references course(id),
  primary key(student_id, course_id)
);
