insert into department (id, name) values (1, 'Finance');
insert into department (id, name) values (2, 'Business');
insert into department (id, name) values (3, 'Economics');

insert into course (id, name) values (1, 'Financial Theory');
insert into course (id, name) values (2, 'MBA Introduction');
insert into course (id, name) values (3, 'Business Model');
insert into course (id, name) values (4, 'Microeconomics');
insert into course (id, name) values (5, 'Macroeconomics');
insert into course (id, name) values (6, 'Game Theory');
insert into course (id, name) values (7, 'CAPM Model');
insert into course (id, name) values (8, 'ETF Theory');
insert into course (id, name) values (9, 'Marketing');
insert into course (id, name) values (10, 'Calculus');

insert into student (id, name, department_id) values (1, 'John', 2);
insert into student (id, name, department_id) values (2, 'Mary', 3);
insert into student (id, name, department_id) values (3, 'Daniel', 1);
insert into student (id, name, department_id) values (4, 'Sam', 3);
insert into student (id, name, department_id) values (5, 'Amy', 3);
insert into student (id, name, department_id) values (6, 'Fiona', 1);
insert into student (id, name, department_id) values (7, 'Jane', 3);

insert into phone (phone_number, student_id) values ('318-902-9328', 1);
insert into phone (phone_number, student_id) values ('651-643-4497', 2);
insert into phone (phone_number, student_id) values ('570-735-7302', 2);
insert into phone (phone_number, student_id) values ('626-324-9283', 4);
insert into phone (phone_number, student_id) values ('213-309-9149', 5);
insert into phone (phone_number, student_id) values ('401-440-8129', 6);
insert into phone (phone_number, student_id) values ('409-267-3522', 6);
insert into phone (phone_number, student_id) values ('508-396-4392', 7);
insert into phone (phone_number, student_id) values ('423-420-4650', 7);

insert into student_course (student_id, course_id) values (1, 2);
insert into student_course (student_id, course_id) values (1, 5);
insert into student_course (student_id, course_id) values (1, 9);
insert into student_course (student_id, course_id) values (1, 10);
insert into student_course (student_id, course_id) values (3, 3);
insert into student_course (student_id, course_id) values (3, 4);
insert into student_course (student_id, course_id) values (4, 1);
insert into student_course (student_id, course_id) values (4, 2);
insert into student_course (student_id, course_id) values (4, 5);
insert into student_course (student_id, course_id) values (4, 6);

insert into student_course (student_id, course_id) values (5, 3);
insert into student_course (student_id, course_id) values (5, 4);
insert into student_course (student_id, course_id) values (5, 5);
insert into student_course (student_id, course_id) values (5, 7);
insert into student_course (student_id, course_id) values (5, 8);
insert into student_course (student_id, course_id) values (6, 8);
insert into student_course (student_id, course_id) values (6, 10);
insert into student_course (student_id, course_id) values (7, 1);
insert into student_course (student_id, course_id) values (7, 7);
insert into student_course (student_id, course_id) values (7, 8);



