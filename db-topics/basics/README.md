# Database Basic Topics

## ORMs
Object-relational mapping (ORM) is a technique for converting data between database and object-oriented programming languages. So we can interact with our database using our language of choice instead of SQL.

### Pros
- You get to write in the language you are already using anyway. We don't need to use SQL.
- It abstracts away the database system so that switching from MySQL to PostgreSQL. So ORM is an interface.
- Depending on the ORM you get a lot of advanced features out of the box, such as support for transactions, connection pooling, migrations, seeds, streams, and all sorts of other goodies.
- Many of the queries you write will perform better than if you wrote them yourself.

### Cons
- If you are a master at SQL, you can probably get more performant queries by writing them yourself.
- There is overhead involved in learning how to use any given ORM.
- The initial configuration of an ORM can be a headache.
- As a developer, it is important to understand what is happening under the hood. Since ORMs can serve as a crutch to avoid understanding databases and SQL, it can make you a weaker developer in that portion of the stack.

### DAOs vs ORMs
Data Access Object (DAO) is an object that abstract the implementation of a persistent data store away from the application and allows for simple interaction with it. A ORM is a robust library that provides a bunch of tools to save/retrieve an object directly to/from the database without having to write your own SQL statements.

## Transactions
A transaction generally represents any change in a database. A database transaction, by definition, must be **atomic** (it must either complete in its entirety or have no effect whatsoever), **consistent** (it must conform to existing constraints in the database), **isolated** (it must not affect other transactions) and **durable** (it must get written to persistent storage), please refer to ACID section.

### States of Transactions

<img src="../screenshots/transactions.png" alt="transactions" />

- Active - In this state, the transaction is being executed. This is the initial state of every transaction.
- Partially Committed − When a transaction executes its final operation, it is said to be in a partially committed state.
- Failed − A transaction is said to be in a failed state if any of the checks made by the database recovery system fails. A failed transaction can no longer proceed further.
- Aborted − If any of the checks fails and the transaction has reached a failed state, then the recovery manager rolls back all its write operations on the database to bring the database back to its original state where it was prior to the execution of the transaction. Transactions in this state are called aborted. The database recovery module can select one of the two operations after a transaction aborts:
    - Re-start the transaction.
    - Kill the transaction.
- Committed − If a transaction executes all its operations successfully, it is said to be committed. All its effects are now permanently established on the database system.

## ACID

In order to maintain consistency in a database, before and after the transaction, certain properties are followed. These are called ACID properties. ACID means **Atomicity**, **Consistency**, **Isolation** and **Durability**.

### Atomicity
Atomicity mean that either the entire transaction takes place at once or doesn't happen at all. There is no midway, transactions do not occur partially. Each transaction is considered as one unit and either runs to completion or is not executed at all. It involves the following two operations.
- **Abort**: If a transaction aborts, changes made to database are not visible.
- **Commit**: If a transaction commits, changes made are visible.

### Consistency
Consistency means that [integrity constraints](https://en.wikipedia.org/wiki/Data_integrity) must be maintained so that the database is consistent before and after the transaction. It refers to the correctness of a database.

### Isolation
Isolation ensures that multiple transactions can occur concurrently without leading to the inconsistency of database state. Transactions occur independently without interference. Changes occurring in a particular transaction will not be visible to any other transaction until that particular change in that transaction is written to memory or has been committed.

### Durability
Durability ensures that once the transaction has completed execution, the updates and modifications to the database are stored in and written to disk and they persist even if a system failure occurs. These updates now become permanent and are stored in non-volatile memory. The effects of the transaction, thus, are never lost.

## N+1 Problem

If we have a purchase order list which includes N orders, and each order consists of an order ID, a customer ID, and one or more items that are being bought. So if we want to find out all items in different orders, we can query like:

```
SELECT id FROM purchase_order;
SELECT * FROM item WHERE purchase_order_id = :id;
```
If we need 1 query for getting all purchase order IDs and N queries for all order IDs to get all items. We need N+1 times queries in total.

### HQL

Let's take a look about N+1 problem in Spring JPA Hibernate.
1. Go to folder [n-plus-1-problem](n-plus-1-problem/).
    ```
    cd n-plus-1-problem
    ```
1. Start PostgreSQL via `docker-compose`.
    ```
    docker-compose up
    ```
1. Open [n-plus-1-problem](n-plus-1-problem/) project by your IDE and run [main()](n-plus-1-problem/src/main/java/cpm/chuck/Application.java) method to start Spring Boot service.
1. Send a GET request to http://localhost:3001/students and see the logs. Hibernate queries `student.id` first, then queries `department` for all departments shown in first query, then queries `phones` and `courses`. So it queries 1 + D + P + C times.
    - D times for querying departments by `department_id`, there are 3 departments, D = 3.
    - P times for querying Phones by `student_id`, there are 7 students, P = 7.
    - C times for querying Courses by `student_id`, there are 7 students, P = 7.
1. In previous step, we know that the default query method is bad, so we should avoid N+1 problem by using `join fetch` statement. Please try to send a GET request to http://localhost:3001/fetch-join-deparment and see the logs. Now we can see the first log as follows. `join fetch` is `inner join` in SQL, so now we query 1 + P + C times.
    ```
    Hibernate: select distinct studentdao0_.id as id1_3_0_, department1_.id as id1_1_1_, studentdao0_.department_id as departme4_3_0_, studentdao0_.name as name2_3_0_, studentdao0_.version as version3_3_0_, department1_.name as name2_1_1_, department1_.version as version3_1_1_ from student studentdao0_ inner join department department1_ on studentdao0_.department_id=department1_.id
    ```
1. Please try to send a GET request to ...
    - http://localhost:3001/fetch-join-phones, it queries 1 + D + C times.
    - http://localhost:3001/fetch-join-courses, it queries 1 + D + P times.
    - http://localhost:3001/fetch-join-department-phones, it queries 1 + C times.
    - http://localhost:3001/fetch-join-department-courses, it queries 1 + P times.
1. You might be interested that why we don't use following query to get all in 1 time. The truth is you will get a `MultipleBagFetchException`. The reason why a `MultipleBagFetchException` is thrown by Hibernate is that duplicates can occur, fetching more than one bag would generate a [Cartesian product](https://en.wikipedia.org/wiki/Cartesian_product). The unordered `List`, which is called a **bag** in Hibernate terminology, is not supposed to remove duplicates.
    ```java
    @Query("select distinct s from StudentDao s join fetch s.department left join fetch s.phones left join fetch s.courses")
    List<StudentDao> fetchJoin();
    ```
1. One possible way is to use `Set` instead of `List`, then SQL will be like as follows. But this might be a bad move, because the final result set will contain 7(students) x 3(departments) x 9(phones) x 10(courses) = 1890 records. That's so terrible from a performance perspective. For more details, please refer to article [The best way to fix the Hibernate MultipleBagFetchException](https://vladmihalcea.com/hibernate-multiplebagfetchexception/).
    ```
    SELECT
        ...
    FROM
        student s
    INNER JOIN
        department d ON s.department_id = d.id
    LEFT OUTER JOIN
        phones p ON s.id = p.student_id
    LEFT OUTER JOIN
        student_course sc ON s.id = sc.student_id
    LEFT OUTER JOIN
        course c ON sc.course_id = c.id
    ```
1. To sum up, we can choose `getByFetchJoinDepartmentAndCourses()` or `getByFetchJoinDepartmentAndPhones()` in [StudentController](n-plus-1-problem/src/main/java/cpm/chuck/StudentController.java). Yes, we can only solve some parts of this complex problem, but we should not replace `List` by `Set` to avoid `MultipleBagFetchException`. Because the result is too large, we need to find a balance.

### Criteria Query

For Criteria API, you will get same results with HQL. You can send a GET request to following URL and see results.
- http://localhost:3001/cq/students.
- http://localhost:3001/cq/fetch-join-department.
- http://localhost:3001/cq/fetch-join-department-courses.

### JPA Specifications

For JPA Specifications, you will get same results with HQL and Criteria Query. But JPA Specifications are used for conditional query, so let's try following GET request.
- http://localhost:3001/specs/students?name=Sam
- http://localhost:3001/specs/students?name=J

After finishing all tests, please do following steps:
1. Stop service.
1. Open a new terminal and go to folder [n-plus-1-problem](n-plus-1-problem/).
    ```
    cd n-plus-1-problem
    ```
1. Stop PostgreSQL via `docker-compose`.
    ```
    docker-compose down
    ```

## Database Normalization

**Normalization** is a database design technique that reduces data redundancy and eliminates undesirable characteristics like insertion, update and deletion anomalies. The inventor of the relational model Edgar Codd proposed the theory of normalization with the introduction of the **First Normal Form (1NF)**, and he continued to extend theory with 2NF and 3NF. Later he joined Raymond F. Boyce to develop the theory of **Boyce-Codd Normal Form (BCNF)**.

### Database Normal Forms
Here is a list of Normal Forms:

- 1NF (First Normal Form)
- 2NF (Second Normal Form)
- 3NF (Third Normal Form)
- BCNF (Boyce-Codd Normal Form)
- 4NF (Fourth Normal Form)
- 5NF (Fifth Normal Form)
- 6NF (Sixth Normal Form)

The Theory of Data Normalization in SQL is still being developed further. However, in most practical applications, normalization achieves its best in 3rd Normal Form. For the following sections, we will illustrate the concept with following example.

Customer|Phone|Purchase Items|Prices|Amount|Sum
--|--|--|--|--|--
John|409-267-3522|toilet paper, shampoo, toothbrush|13, 12, 2|1, 1, 3|31
Susan|508-396-4392|facial cleanser|15|2|30
Susan|651-643-4497|shaving cream, shampoo|10, 12|1, 1|22

### 1NF
- Each table cell should contain a single value.
- Each record needs to be unique.

We can see more than 1 value in **Purchase Items** column, so we fix the above table in 1NF:

Customer|Phone|Purchase Item|Price|Amount|Sum
--|--|--|--|--|--
John|409-267-3522|toilet paper|13|1|13
John|409-267-3522|shampoo|12|1|12
John|409-267-3522|toothbrush|2|3|6
Susan|508-396-4392|facial cleanser|15|2|30
Susan|651-643-4497|shaving cream|10|1|10
Susan|651-643-4497|shampoo|12|1|12

### 2NF
- It should be in the 1NF.
- Single Column Primary Key.

In our database, you can notice that we have 2 people with same name "Susan". We should use composite key composed of **Customer** and **Phone**. So we partition the table above.

Customer ID|Customer|Phone
--|--|--
1|John|409-267-3522
2|Susan|508-396-4392
3|Susan|651-643-4497

Customer ID|Purchase Item|Price|Amount|Sum
--|--|--|--|--
1|toilet paper|13|1|13
1|shampoo|12|1|12
1|toothbrush|2|3|6
2|facial cleanser|15|2|30
3|shaving cream|10|1|10
3|shampoo|12|1|12

### 3NF
- It is in the 2NF.
- It doesn't have **Transitive Dependency**.

**Transitive Dependency** means when a non-prime attribute depends on other non-prime attributes rather than depending upon the prime attributes or primary key. We can notice that the **Sum** depends on **Prices** and **Amount**, that's so-called transitive dependency. We can remove transitive dependency by removing **Sum**.


Customer ID|Customer|Phone
--|--|--
1|John|409-267-3522
2|Susan|508-396-4392
3|Susan|651-643-4497

Customer ID|Purchase Item|Price|Amount
--|--|--|--
1|toilet paper|13|1
1|shampoo|12|1
1|toothbrush|2|3
2|facial cleanser|15|2
3|shaving cream|10|1
3|shampoo|12|1

### BCNF (Boyce-Codd Normal Form)

- It should be in the 3NF.
- For a dependency A → B, A cannot be a non-prime attribute, if B is a prime attribute.

Sometimes is BCNF is also referred as 3.5 NF. Let's see our database, you can notice that **Price** depends on **Purchase Item**, but **Purchase Item** is one of prime attribute and also is a part of primary key, so we don't violate 3NF. But it's not in BCNF. We can fix it as follows.

Customer ID|Customer|Phone
--|--|--
1|John|409-267-3522
2|Susan|508-396-4392
3|Susan|651-643-4497

Customer ID|Purchase Item ID|Amount
--|--|--
1|1|1
1|2|1
1|3|3
2|4|2
3|5|1
3|2|1

Purchase Item ID|Purchase Item|Price
--|--|--
1|toilet paper|13
2|shampoo|12
3|toothbrush|2
4|facial cleanser|15
5|shaving cream|10

## Indices
A database index is a data structure that improves the speed of data retrieval operations on a database table at the cost of additional writes and storage space to maintain the index data structure. For the content in this section, please refer to [here](https://www.geeksforgeeks.org/indexing-in-databases-set-1/). 

Indexes are created using a few database columns.

- The first column is the Search key that contains a copy of the primary key or candidate key of the table. These values are stored in sorted order so that the corresponding data can be accessed quickly.
Note: The data may or may not be stored in sorted order.
- The second column is the Data Reference or Pointer which contains a set of pointers holding the address of the disk block where that particular key value can be found.

### File Organization Mechanism
In general, there are two types of file organization mechanism which are followed by the indexing methods to store the data:

1. **Sequential File Organization** or **Ordered Index File**: In this, the indices are based on a sorted ordering of the values. These are generally fast and a more traditional type of storing mechanism. These Ordered or Sequential file organization might store the data in a dense or sparse format:
    - **Dense Index**:
        <img src="../screenshots/dense-index.jpg" alt="dense-index"/>
        - For every search key value in the data file, there is an index record. 
        - This record contains the search key and also a reference to the first data record with that search key value.
        
    - **Sparse Index**: 
        <img src="../screenshots/sparse-index.jpg" alt="sparse-index"/>
        - The index record appears only for a few items in the data file. Each item points to a block as shown.
        - To locate a record, we find the index record with the largest search key value less than or equal to the search key value we are looking for.
        - We start at that record pointed to by the index record, and proceed along with the pointers in the file (that is, sequentially) until we find the desired record.

1. **Hash File organization**: Indices are based on the values being distributed uniformly across a range of buckets. The buckets to which a value is assigned is determined by a function called a hash function.

### Index Architecture
There are primarily 3 methods of indexing.
1. **Clustered**. When more than 2 records are stored in the same file, these types of storing is known as cluster indexing. By using the cluster indexing we can reduce the cost of searching, because multiple records related to the same thing are stored at one place and it also gives the frequent join of more than 2 tables(records).<br><br>
Clustering index is defined on an ordered data file. The data file is ordered on a non-key field. In some cases, the index is created on non-primary key columns which may not be unique for each record. In such cases, in order to identify the records faster, we will group 2 or more columns together to get the unique values and create index out of them. This method is known as the clustering index. Basically, records with similar characteristics are grouped together and indexes are created for these groups.<br><br>
**Primary Indexing** is a type of Clustered Indexing wherein the data is sorted according to the search key and the primary key of the database table is used to create the index. It is a default format of indexing where it induces sequential file organization. As primary keys are unique and are stored in a sorted manner, the performance of the searching operation is quite efficient.

1. **Non-clustered**. A non clustered index just tells us where the data lies, i.e. it gives us a list of virtual pointers or references to the location where the data is actually stored. Data is not physically stored in the order of the index. Instead, data is present in leaf nodes. e.g. the contents page of a book. Each entry gives us the page number or location of the information stored. The actual data here(information on each page of the book) is not organized but we have an ordered reference(contents page) to where the data points actually lie. We can only have dense ordering in the non-clustered index, as sparse ordering is not possible because data is not physically organized accordingly.<br>
It requires more time as compared to the clustered index because some amount of extra work is done in order to extract the data by further following the pointer. In the case of a clustered index, data is directly present in front of the index.
    <img src="../screenshots/non-clustered.png" alt="non-clustered"/>

1. **Multilevel Indexing**. With the growth of the size of the database, indices also grow. As the index is stored in the main memory, a single-level index might become too large a size to store with multiple disk accesses. The multilevel indexing segregates the main block into various smaller blocks so that the same can stored in a single block. The outer blocks are divided into inner blocks which in turn are pointed to the data blocks. This can be easily stored in the main memory with fewer overheads.
    <img src="../screenshots/multilevel-indexing.png" alt="multilevel-indexing"/>
